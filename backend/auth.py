import logging
import webapp2
import json

from google.appengine.ext import db

from google.appengine.api import memcache
from google.appengine.api import users

from backend.models import Person, Script
from backend.util import EXAMPLE_CONTENT



def decode_name(email, headers):
    name = email.split('@')[0]
    charsets = headers['Accept-Charset']
    if charsets:
        charsets = charsets.split(';')[0].split(',')
    else:
        charsets = ['utf-8', 'iso-8859-1']
    
    for charset in charsets:
        try:
            return unicode(name, charset)
        except UnicodeDecodeError:
            continue
        except LookupError:
            continue

    raise ValueError("Bad username: bad charset")

def get_or_insert_person(user, request):
    uid = user.user_id()
    p = Person.get_by_key_name(uid)
    if p:
        logging.info("user login: %s ; key: %s" % (p.name, p.key()))
        return (p, None)

    name = decode_name(user.email(), request.headers)
    if Person.all().filter("name =", name).fetch(1):
        raise ValueError("Bad user: username already in use.")
    
    p = Person(key_name=uid, name=name, email=user.email())
    p.put()

    s = request.get('script')
    if s:
        s = json.loads(s)
        path = s['path'].replace(s['creator'], p.name)
        content = s['content']
    else:
        path = "/" + p.name + "/example.coffee"
        content = EXAMPLE_CONTENT

    new_s = Script(key_name=path, parent=p, creator=p.name,
                   path=path, content=content)
    new_s.put()
    logging.info("user register: %s ; key: %s" % (p.name, p.key()))
    return (p, new_s)


class LoginHandler(webapp2.RequestHandler):

    def get(self):
        self.post()
    
    def post(self):
        user = users.get_current_user()
        o = self.response.out
        if not user:
            o.write( users.create_login_url("/") )
            return

        cache_key = "login_data:" + str(user.user_id())
        data = memcache.get(cache_key)
        if data:
            o.write(data)
            return

        p, s = get_or_insert_person(user, self.request)

        if not p:
            raise ValueError("Invalid user: Please try a different account.")

        url = users.create_logout_url("/")
        data = url + "\n"
        data += p.name + "\n"
        data += str(p.key()) + "\n"

        memcache.set(cache_key, data)

        if s:
            # this is only done on first login so we don't cache it
            logging.info("appending redirect path: " + s.path)
            # this is when user was created
            data += s.path + "\n"

        o.write(data)


app = webapp2.WSGIApplication([('/auth/', LoginHandler)], debug=True)



