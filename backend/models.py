import re
import time
import logging

import json

from xml.sax.saxutils import escape

from google.appengine.ext import db


SCRIPT_FORMATS = set(["coffeescript", "javascript", "processing"])
#SCRIPT_TYPES = set(['pcs', 'pjs', 'pde', 'json'])
HEADER_RE = re.compile("\s*^###(.*?)###", re.MULTILINE|re.DOTALL)


class Person(db.Model):
    
    name    = db.StringProperty(required=True)
    email   = db.StringProperty(required=True)
    has_published = db.BooleanProperty(default=False)

    
class Script(db.Model):
    
    path     = db.StringProperty(required=True)
    src_path = db.StringProperty()
    creator  = db.StringProperty(required=True)
    
    format   = db.StringProperty(default="coffeescript", required=True,
                                 choices=SCRIPT_FORMATS)
    content  = db.TextProperty(default="")
    
    flagged  = db.BooleanProperty(default=False)
    updated  = db.DateTimeProperty(auto_now=True)
    image    = db.BlobProperty()
    image_type = db.StringProperty(default="png")

    def __str__(self):
        return u"\n".join((
            self.path,
            self.creator,
            str(self.updated_time),
        ))

    def short_dict(self):
        summary = ""

        m = HEADER_RE.match(self.content)

        if m:
            summary = m.groups()[0].replace("\r\n", "\n")
            summary = summary.replace("\n# ", "\n").replace("\n#", "\n")
            summary = escape(summary)

        return {
            'path'   : self.path,
            'creator': self.creator,
            'updated': self.updated_time,
            'has_img': self.image != None,
            'summary': summary,
        }

    def as_json(self):
        return json.dumps(self.as_dict())
    
    def as_dict(self):
        return {
            'path'   : self.path,
            'creator': self.creator,
            'format' : self.format,
            'content': self.content,
            'flagged': self.flagged,
            'updated': self.updated_time,
        }
    
    @property
    def updated_time(self):
        return int(time.mktime(self.updated.timetuple()))

class Rating(db.Model):
    
    rating  = db.RatingProperty() # 1-100
    user    = db.UserProperty(required=True, auto_current_user=True)
    created = db.DateTimeProperty(auto_now=True)

