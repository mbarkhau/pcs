import base64
import logging

import webapp2

from datetime import datetime

from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.api import images

from backend.models import Person, Script
from backend.util import get_current_person

EMPTY_GIF = base64.decodestring("R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==")


def resize(img):
    lx = ty = 0.0
    rx = by = 1.0
    w = img.width
    h = img.height
    if w > (h * 1.6) :
        lx = (w - (h * 1.6)) / (w * 2)
        rx = 1.0 - lx
    else:
        ty = (h - (w / 1.6)) / (h * 2)
        by = 1.0 - ty

    img.crop(lx, ty, rx, by)
    img.resize(256, 160)
    return img.execute_transforms(images.JPEG)


class ImageHandler(webapp2.RequestHandler):

    def get(self, path):
        s = Script.all().filter("path = ", path).get()
        if not s:
            return self.error(404)

        r = self.response

        etag = self.request.headers.get('If-None-Match', "")
        cur_etag = s.updated.strftime("%j%H%M%S")

        if cur_etag == etag or not s.image and etag == '0000000':
            r.set_status(304)
            r.headers['ETag'] = '0000000'
            return

        if s.image:
            r.headers['Content-Type'] = 'image/' + s.image_type
            r.headers['ETag'] = cur_etag
            r.out.write(s.image)
        else:
            r.headers['Content-Type'] = 'image/gif'
            r.headers['ETag'] = '0000000'
            r.out.write(EMPTY_GIF)

    def post(self, path):
        s = Script.all().filter("path = ", path).get()
        
        p = get_current_person()
        if not p or p.name != s.creator:
            return self.error(503)

        if not s:
            return self.error(404)

        img_data = self.request.get('img_data')[22:]
        img = base64.decodestring(img_data)
        img = resize(images.Image(img))

        s.image = db.Blob(img)
        s.image_type = 'jpeg'
        s.put()


app = webapp2.WSGIApplication([('/images(/.*)', ImageHandler)], debug=True)

