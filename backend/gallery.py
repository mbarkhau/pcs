import logging
import webapp2
import json
from datetime import datetime

from backend.models import Person, Script

PAGESIZE = 20

class GalleryHandler(webapp2.RequestHandler):

    def get(self, next=None):
        r = self.response
        r.headers['Content-Type'] = 'application/json;charset=utf-8'

        scripts = Script.all().order("-updated")
        newest = scripts.get()
        res = [newest.updated_time]

        if next and next.isdigit():
            next = datetime.fromtimestamp(int(next) + 1)
            scripts = scripts.filter('updated < ', next)

        scripts = scripts.fetch(PAGESIZE)
        for s in scripts:
            res.append(s.short_dict())

        r.out.write(json.dumps(res))


app = webapp2.WSGIApplication([('/gallery/(.*)', GalleryHandler)], debug=True)
