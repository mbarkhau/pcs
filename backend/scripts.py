import re
import json
import logging
import webapp2

from cgi import parse_qsl

from google.appengine.ext import db
from google.appengine.api import memcache

from backend.models import Person, Script
from backend.util import get_current_person


def update_cache(script):
    data = script.as_json()

    memcache.set("script_" + script.path, data)
    return data

PATH_RE = re.compile("\/\w+\/(\w+\.\w+)")

def is_valid_path(path, person):
    if not path.startswith("/"):
        return False
    if not path.startswith("/" + person.name):
        return False
    if path in ["/" + person.name, "/" + person.name + "/"]:
        return False
    if not PATH_RE.match(path):
        return False
    #if not path.endswith('pcs'):
    #    return False
    return True

class ScriptHandler(webapp2.RequestHandler):

    def get(self):
        path = self.request.get('path')
        cache_key = "script_" + path
        data = memcache.get(cache_key)
        if not data:
            s = Script.all().filter("path = ", path).get()
            if not s:
                return self.error(404)

            data = update_cache(s)

        self.response.out.write(data)

    def put(self):
        args = dict(parse_qsl(self.request.body))
        action = args['action']
        path = args['path']
        new_path = args['new_path']

        p = get_current_person()
        if not is_valid_path(new_path, p):
            return self.error(400)

        old_s = Script.all().filter("path = ", path).get()
        if not old_s:
            self.error(404)
            return 

        def put_script(old_s):
            if action in ['rename', 'copy']:
                exist = db.Key.from_path('Script', new_path, parent=p.key())
                if db.get(exist):
                    self.error(409)
                    return

            if action in ['rename', 'publish']:
                if old_s.creator != p.name:
                    self.error(503)
                    return

            if action == 'rename':
                old_s = db.get(db.Key.from_path('Script', path, parent=p.key()))
                s = Script(key_name=new_path, parent=p, creator=p.name,
                           path=new_path, src_path=old_s.src_path,
                           content=old_s.content)
                old_s.delete()
                logging.info("renamed '%s' to '%s'" % (old_s.path, new_path))

            elif action == 'copy':
                s = Script(key_name=new_path, parent=p, creator=p.name, 
                           path=new_path, src_path=path,
                           content=old_s.content)
                logging.info("copied '%s' to '%s'" % (old_s.path, new_path))

            s.put()
            return s

        s = db.run_in_transaction(put_script, old_s)
        if not s:
            # this means an error happend
            return

        data = update_cache(s)
        self.response.out.write(data)

        
    def post(self):
        """ Create/Save a script

        Called on every ctrl+s
        """
        r = self.request

        p = get_current_person()

        path = r.get('path')
        if not is_valid_path(path, p):
            return self.error(400)

        content = r.get('content')

        def save_script():
            s = db.get(db.Key.from_path('Script', path, parent=p.key()))
            if s is None:
                s = Script(key_name=path, parent=p, path=path,
                           creator=p.name, content=content)
            else:
                s.content = content
            s.put()
            return s

        s = db.run_in_transaction(save_script)
        update_cache(s)

        logging.info("saved: " + path)
        self.response.out.write("saved")

        
app = webapp2.WSGIApplication([('/scripts/', ScriptHandler)], debug=True)

