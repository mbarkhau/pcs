from google.appengine.api import users
from backend.models import Person

def get_current_person():
    user = users.get_current_user()
    if not user:
        return None
    return Person.get_by_key_name(user.user_id())


EXAMPLE_CONTENT = """
###
# Example code adapted from http://processingjs.org/learning
###

delay = 50
X = pcs.width  / 2
Y = pcs.height / 2

###
# Setup the Processing Canvas
###
setup = ->
    strokeWeight( 10 )
    fill(255, 200, 0)    # Set fill-color to orange
    stroke(0, 100, 200)  # Set stroke-color to blue

###
# Main draw loop
###
draw = ->
    background( 255 )     # overwrite previous content

    deltaX = pcs.mouseX-X
    deltaY = pcs.mouseY-Y
    delta  = sqrt((deltaX*deltaX) + (deltaY*deltaY))

    # Track circle to new destination
    X += deltaX / delay
    Y += deltaY / delay

    pulse = sin( pcs.frameCount / 20 ) * 10
    radius = 50 + (delta / 8) + pulse

    ellipse( X, Y, radius, radius ) # Draw circle

"""


