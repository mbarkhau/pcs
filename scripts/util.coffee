win_dims = -> [window.innerWidth, window.innerHeight]


log = ->
    msg = (for a in arguments then a).join(", ")
    Processing?.logger?.log(msg)
    console.log("debug:", arguments)
    return

print = -> log(arguments...)

# I can't be bothered to figure out a better way
close_log = -> $('div[title="Close Log"]').click()
logger_panel = -> $('div[title="Close Log"]').parent()

Array::last = -> this[@length-1]
String::startsWith = (str) -> @indexOf(str) == 0
String::endsWith = (str) -> new RegExp("#{str}$").test(this)
String::isUpper = -> 64 < @charCodeAt(0) < 91
String::times = (n)->
    return "" if not n
    str = this
    while n -= 1 then str += this
    return str

NAME_RE = /(\/[\w\.]+\/)([^\.]+)(\.\w+)?/
path_to_title = (p) -> NAME_RE.exec(p)?[2].titleize() or ""
    
String::titleize = ->
    arr = @split(/[ _\-\/]/)
    for word, i in arr
        word = word.split('')
        if word[0]?
            word[0] = word[0].toUpperCase()

        arr[i] = if i+1 == arr.length
            word.join('')
        else
            word.join('') + ' '
      return arr.join('')
      
String::trim = -> String(this).replace(/^\s+|\s+$/g, '')

QUERY_STRING_RE = new RegExp("([^?=&]+)(=([^&#]*))?", "g")
QUERY_STRING_RE = /([^?=&]+)(=([^&#]*))?/g

decodeQueryString = ->
    queryString = {}
    location.href.replace(
        QUERY_STRING_RE, 
        ($0, $1, $2, $3) -> queryString[$1] = $3
    )
    return queryString

PATH_RE = /\/[\w\.]+\/(\w+\.\w+)/
isValidPath = (p) -> p.match(PATH_RE)

# debug funcs
window.get_script = (path) -> db.get(path)
window.keys = -> (for k of db._ then k)

jQuery.fn.selectPath = ->
    v = @val()

    e = v.length
    
    m = v.match(NAME_RE)
    s = if m and m[2]+m[3] then m[1].length else 0

    i = this[0]
    if i.createTextRange
        r = i.createTextRange()
        r.collapse(true)
        r.moveStart('character', s)
        r.moveEnd('character', e-s)
        r.select()
    else if i.setSelectionRange
        i.setSelectionRange(s, e)
    else if field.selectionStart
        i.selectionStart = s
        i.selectionEnd = e
    

# Takes a Date object
prettyDate = (date) ->
    diff = new Date() - date
    flr = Math.floor
    return if isNaN(diff) or diff < 0
            
    return (
        diff < MIN and "just now" or
        diff < 2*MIN and "1 minute ago" or
        diff < HOUR and flr( diff / MIN ) + " minutes ago" or
        diff < 2*HOUR and "1 hour ago" or
        diff < DAY and flr( diff / HOUR ) + " hours ago" or
        diff < 2*DAY and "Yesterday" or
        diff < WEEK and flr(diff / DAY) + " days ago" or
        diff < 30*DAY and flr( diff / WEEK ) + " weeks ago" or
        diff < 2*YEAR and flr( diff / (30*DAY) ) + " months ago" or
        flr( diff / YEAR ) + " years ago"
    )

