db = kizzy('pcs')

cur_script = -> db.get(cur_path())

last_script = (s) ->
    if s
        db.set('last_script', s)
    else
        db.get('last_script')

clear_cache = ->
    for own k of db._
        if k.startsWith("/")
            db.remove(k)
    return

