###
# normalize path for current user
###
norm_path = (path) ->
    #if not path.match(/\.pcs$/)
    #    path += ".pcs"
    if not path.startsWith("/")
        path = "/" + path
    if not path.startsWith("/" + pcs.username)
        path = "/" + pcs.username + path

    return path

###
make sure the editor restyles when it regains focus
###
editor_touch = ->
    ui = pcs.editorUI
    return if not ui
    ui.focus()
 

_error_holder = null # to keep msg around until editor loaded again
_error_marker = null

editor_load = ->
    editor_init() if not pcs.editorUI
    ui = pcs.editorUI
    s = cur_script()
    
    if ui.getValue() != s.content # preserve history when possible
        ui.setValue(s.content)

    editor_mark_error()

    editor_scroll(s.scrollTop or 0)
    editor_cursor(s.cursor)
    editor_touch()

editor_cursor = (cur) ->
    ui = pcs.editorUI
    if cur
        ui.setCursor(cur)
    else
        ui.getCursor()

editor_scroll = (pos) ->
    # notice how when pos is undefined scrollTop will return current pos
    $(pcs.editorUI.getScrollerElement()).scrollTop(pos)


error_check = ->
    [code, err] = errWrappedCompile(cur_path())
    _error_holder = err
    return err

editor_mark_error = ->
    ui = pcs.editorUI
    if _error_marker
        ui.clearMarker(_error_marker)
    return if not _error_holder

    err = String(_error_holder)
    log(err)
    line = err.match(/line (\d+)/i)
    if line
        line = parseInt(line[1]) - 1
        _error_marker = ui.setMarker(line, "%N%", 'errorline')
        ui.setCursor({line:line, ch: 0})

    ui.focus()
    
    setTimeout((-> $('.errorline').attr('title', err)), 200)

editor_cache = ->
    ui = pcs.editorUI
    s = cur_script()
    if ui? and s?
        s.content = ui.getValue()
        last_script(s)
        s.scrollTop = editor_scroll()
        s.cursor = editor_cursor()
        s
    else
        last_script() or s

editor_halt = ->
    editor_cache()
    if pcs.userkey and cur_username() == pcs.username
        editor_save()
    else
        error_check()

get_valid_userpath = ->
    ALT_MSG = """
    Filename in use or invalid.

    Please choose a different name.
    """
    # initial message is replaced on repeated calls
    msg = "Please choose a filename."

    path = norm_path(cur_subpath() or "/hello.coffee")
    invalid_paths = db.get('mypaths')
    invalid_paths.push("/" + pcs.username)
    invalid_paths.push("/" + pcs.username + "/")
    
    while (path in invalid_paths) or not isValidPath(path)
        path = prompt(msg, path)
        msg = ALT_MSG
        break if path == null
        path = norm_path(path)

    return path

editor_rename = ->
    path = get_valid_userpath()
    return if not path
    
    rename_script(cur_path(), path, view_switcher('edit', path))

editor_copy = ->
    path = get_valid_userpath()
    return if not path

    copy_script(cur_path(), path, view_switcher('edit', path))

editor_new = ->
    path = get_valid_userpath()
    return if not path
    copy_script("/mbarkhau/hello.coffee", path, view_switcher('edit', path))

editor_save = ->
    btn = $('.button.save span')
    toggleErrImg = ->
        btn.removeClass('spinner')
        btn.toggleClass('page_save')
        btn.toggleClass('exclamation')
    save_ok = (data, status) ->
        btn.removeClass('spinner')
    save_fail =->
        toggleErrImg()
        setTimeout(toggleErrImg, 1500)

    s = cur_script()

    ui = pcs.editorUI
    if pcs.username
        if ui and s.creator == pcs.username
            s.content = ui.getValue()
            s.scrollTop = editor_scroll()
            db.set(s.path, s)

            btn.addClass('spinner')
            save_script(s, save_ok, save_fail)

        if s.creator != pcs.username
            MSG = """
            Sorry, this script belongs to "#{s.creator}".

            Instead we will make a copy with your changes.
            """
            copy_ok = (data, status) ->
                save_ok(data, status)
                view_switch('edit', path)

            if confirm(MSG) and (path = get_valid_userpath())
                s.content = ui.getValue()
                s.path = path
                save_script(s, copy_ok, save_fail)
    else
        alert("""
        Sorry, login required to save script.

        Warning: Any current changes will be lost.
        You may wish to save them locally before logging in.
        """)

    error_check()
    editor_mark_error()
    return s.content

_prev_line = 0
    
highlight_line = ->
    ui = pcs.editorUI
    
    ui.setLineClass(_prev_line, null)
    
    _prev_line = ui.getCursor().line
    ui.setLineClass(_prev_line, "activeline")

editor_init = ->
    args = {
        indentUnit      : 4
        indentWithTabs  : false
        tabMode         : 'classic'
        enterMode       : 'indent'
        lineNumbers     : true
        matchBrackets   : true
        saveFunction    : editor_save
        mode            : 'coffeescript'
        theme           : 'default'
        onCursorActivity: highlight_line
        onKeyEvent      : onComplete
        undoDepth       : 500
    }
    
    ui = pcs.editorUI = new CodeMirror($('#editor')[0], args)

