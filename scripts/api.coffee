
cache_script = (script) ->
    db.set(script.path, script)
    return if script.creator != pcs.username

    paths = db.get('mypaths') or []
    paths.push(script.path)
    db.set('mypaths', ender.compact(ender.uniq(paths)).sort())
    return script


auth_setup = ->
    auth_button = (login=false, url) ->
        s = $(".button.login")
        h = $(".button.logout")

        if login
            load_myfiles()
            [h, s] = [s, h]
            s.attr('title', "Logged in as '#{pcs.username}' (click to logout)")

        h.hide()
        s.show()

        if url
            s.attr('href', url)

    setup = (data, status, xhr) ->
        #print data
        [url, pcs.username, pcs.userkey, redirect] = data.split('\n')
        #print url, "|", pcs.username, "|", pcs.userkey, "|", redirect

        # The url can either be for logging in or for logging out
        # depending on the users status

        logged_in = pcs.userkey?
        auth_button(logged_in, url)
        db.set('has_account', logged_in)

        if redirect
            view_switch('edit', redirect)

    auth_button() # set initial state

    data = {'script': JSON.stringify(db.get('last_script')) }
    if db.get('has_account') or not data
        $.get("/auth/", null, setup)
    else
        $.post("/auth/", data, setup)

load_docs = -> $.getJSON("/static/docs.json", (docs) -> pcs.docs = docs )

load_suggestions = (q, cb) ->
    opts = { 'q': q, 'nolimit': "true" }
    $.get("/suggest/", opts, (data) -> cb(data.split("\n")))

load_myfiles = ->
    loaded = (paths) ->
        if not db.get('curfile')
            db.set('curfile', "/#{pcs.username}/example.coffee")
        db.set('mypaths', paths)
    load_suggestions("/" + pcs.username + "/", loaded)
    
load_script_callbacks = { } # keeps callbacks

load_script = (path, cb=(->), force=false) ->
    if force
        s = db.get(path)
        return cb(s) if s?

    lsc = load_script_callbacks
    if path not in lsc
        lsc[path] = []

    loading = lsc[path]
    loading.push(cb)

    if loading.length > 1
        return

    loaded = (script, status, xhr) ->
        script = cache_script(script)
        while cb = loading.pop()
            cb(script)

    $.getJSON("/scripts/", { 'path': path }, loaded)

save_script = (script, ok_cb, fail_cb) ->
    xhr = $.ajax({
        url : "/scripts/"
        type: 'POST'
        data: script
    }).success(ok_cb).error(fail_cb)

copy_script = (path, new_path, cb) ->
    update_script('copy', {'path': path, 'new_path': new_path}, cb)

rename_script = (path, new_path, cb) ->
    update_script('rename', {'path': path, 'new_path': new_path}, cb)

update_script = (action, data, cb) ->
    data['action'] = action
    $.ajax({
        url : "/scripts/"
        type: 'PUT'
        data: data
        success: (response) ->
            cache_script(response)
            cb()
    })


load_gallery = (offset, cb) ->
    parser = (items, status) ->
        newest = items[0]
        cb(newest, items[1...])

    $.getJSON("/gallery/#{offset}", null, parser)

