REFS = ""

for n in PCS_USER_FUNCS
   REFS += """
   var nodef = typeof #{n} === 'undefined';
   var nofunc = typeof pcs.#{n} === 'undefined';
   if (nodef && nofunc) {
       pcs.#{n} = function(){};
   } else if (nofunc) {
       pcs.#{n} = #{n};
   }
   """

remove_guard = (code) ->
    lines = code.split("\n")
    lines = lines[1...lines.length-2]
    return lines.join("\n")

###
# The guard is needed for imported modules
###
add_guard = (code) -> "(function(){\n" + code + "\n\n}).call(this);"
    
###
# make processing functions available during evaluation
###
add_proc_refs = (code) ->
    code = remove_guard(code)
    lines = ["var pcs = Processing.instances[0];"]

    pcs = Processing.instances[0]
    for n of pcs when n not in PCS_USER_FUNCS
        # that's some black magic!
        lines.push("var #{n} = pcs.#{n};")

    lines = lines.concat([
        "var require = pcs_require;"
        "var _ = window.ender._;"
        code
    ])
    return add_guard(lines.join("\n"))

compile = (path)->
    code = db.get(path).content
    code = CoffeeScript.compile(code) # --bare option?
    return add_proc_refs(code)

###
# evaluates the file at path
# 
# assumes file is available in db
###
window.pcs_require = (path) ->
    code = remove_guard(compile(path))
    code = add_guard(code + "\nreturn exports;")
    return eval(code)


error_marker = null

errWrappedCompile = (path) ->
    e = pcs.editorUI
    try
        return [compile(path), false]
    catch err
        return [null, err]

REQUIRE_RE = /^[^#][\s\S]*require(\(| )?['"](.*)['"]\)?/
COMMENT_RE = /###/

###
# Recursively scans files for calls to "require" and
# invokes cb when all are in cache
###
load_deps = (path, cb) ->

    find_deps = (path) ->
        s = db.get(path)
        return [] if not s?

        deps = []

        in_comment = false
        for line in s.content.split("\n")
            if line.match(COMMENT_RE)
                in_comment = not in_comment
            
            match = REQUIRE_RE.exec(line)
            if 0 <= line.indexOf("#") < line.indexOf("require")
                continue
            
            continue if in_comment or not match?

            deps.push(match[2])

        return deps

    all_deps = (path) ->
        deps = find_deps(path)
        res = [path]
        for d in deps
            res = res.concat(all_deps(d))
        return res

    ### returns all unloaded *known* dependencies ###
    unloaded_deps = (paths) ->
        for d in paths when not db.get(d)? then d

    loading = {}

    # recursively loads scripts until dependencies are resolved
    # runs the callback when finished
    load_and_run = ->
        all_loaded = true
        deps = all_deps(path)

        for d in unloaded_deps(deps)
            if not loading[d]
                load_script(d)
                loading[d] = true

            all_loaded = false

        if all_loaded
            cb()
        else
            setTimeout(load_and_run, 50)

    load_and_run()


RENDER_MODE_RE = /size\((.+?)\, (.+?)\, (P3D|OPENGL).*;/

###
# Dependencies must be available in cache at this point
###
processingInit = ->
    # We replace the canvas in case the previous one contained
    # data from a different origin. This way the canvas isn't
    # polluted indefinitely/between sketches.
    # This happens for example when loading an image from a
    # different domain.
    canvas = $('<canvas id="canvas"></canvas>').replaceAll('#canvas')

    setup_code = (pcs) ->
        _code = compile(cur_path())
        
        # Size may only be called once for 3d renderers
        # If it is called in user code, it should not be called here
        [width, height] = win_dims()

        _match = _code.match(RENDER_MODE_RE)
        if _match
            [_sizeline, _w, _h, _mode] = _match
            _code = _code.replace(_sizeline, "")
            eval("pcs.size(#{_w}, #{_h}, pcs.#{_mode});")
        else
            pcs.size(width, height)

        pcs.frameRate(30)

        pcs.background(255)
        pcs.stroke(0)
        pcs.fill(0)

        canvas.focus()

        eval(remove_guard(_code))
        eval(REFS) # attach references of code to pcs object

    pcs.processing = new Processing(canvas[0], setup_code)

runtime_start = ->
    path = cur_path()
    [code, errors] = errWrappedCompile(path)
    return if errors

    load_deps(path, processingInit)

runtime_halt = -> pcs.processing?.exit()

screenshot = ->
    img_data = $('#canvas')[0].toDataURL('image/png')
    path = "/images" + cur_path()
    btn = $('.button.screenshot span')
    show_img = ->
        btn.removeClass('spinner')
        open(path, path, "width=256,height=160")
        
    btn.addClass('spinner')
    $.post(path, { 'img_data': img_data }, show_img)



