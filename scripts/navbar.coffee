sug = {
    all: [] # all paths that were suggested
    last: [] # paths suggested for last query
    finished: "" # query.term for result in last
    waiting: "" # query.term currently in flight
}

suggest_path = (query, result_cb) ->
    term = query.term

    if term == sug.finished
        return result_cb(sug.last)

    return if sug.waiting == term
        
    sug.waiting = term

    sug_parser = (data, status) ->
        pnode = $(".script-path")
        if status == 'error' or not data
            pnode.css('background', '#faa')
            return
        else
            # minor hack to fix styling of autocomplete plugin
            pnode.css('background', 'white')

        sug.last = ender.compact(data.split("\n"))
        sug.all = ender.uniq(sug.all.concat(sug.last)).sort()
        sug.finished = term

        result_cb(sug.last)
        sug.waiting = null

    $.get("/suggest/", {'q': term}, sug_parser)

open_script = (path) ->
    load_script(path, (-> 
        view_switch('edit', path)
        $("input.script-path").autocomplete('close').blur()
        pcs.editorUI?.focus()
    ))

isValidSugestion = (path) -> isValidPath(path) and path in sug.all

complete_or_open = (e, ui) ->
    pnode = $("input.script-path")

    if ui
        path = ui.item.value
        pnode.val(path)
    else
        path = pnode.val()
        
    if e
        e.stopPropagation()
        e.preventDefault()

    if isValidSugestion(path)
        open_script(path)
        return false

    last = sug.last
    if last.length == 1 and last[0]?.startsWith(path)
        pnode.val(last[0])

    # trigger further suggestions
    pnode.autocomplete('search')
    return false

init_suggest = ->
    pnode = $("input.script-path")

    pnode.autocomplete({
        'source': suggest_path
        'delay' : 200
        'select': complete_or_open
    }).bind('keydown', (e) ->
        path = pnode.val()
        if e.which == 8 and path.endsWith("/")
            # remove path part on backspace at slash
            path = path.replace(/(.*?)([^\/]*\/$)/, "$1")
            pnode.val(path)
            e.preventDefault()
            return false
        if e.which == 13
            return complete_or_open()
    ).focus((e) ->
        pnode.autocomplete('search')
        pnode.selectPath()
    ).focusout((e) ->
        if not isValidSugestion(pnode.val())
            update_path() # replaces with path of script (if open)
    )


navbar_configure = (cfg, hidepath) ->
    $(".buttons-left a.button").hide()
    for id in cfg
        $("a.button.#{id}").show()
        
    p = $("input.script-path")
    if hidepath
        p.hide()
    else
        p.show()

navbar_show = (show=true) ->
    bar = $("#viewNav")
    btn = $(".navbar-show")

    if show
        bar.show()
        btn.hide()
    else
        bar.hide()
        btn.show()
    
navbar_toggle = ->
    show = $("#viewNav").css('display') != 'none'
    db.set('navbar', !show)
    navbar_show(!show)

###
keeps navbar path in sync with browser path
###
update_path = (path=cur_path()) ->
    title = "Processing CoffeeScript Editor"

    if isValidPath(path) and path and path != "/"
        $('.script-path').val(path)
        db.set('lastpath', path)
        title = path_to_title(path) + " - " + title

    $('title').html(title)


navbar_init = ->
    buttons = [
#      ['id'        , "title text"    , 'icon'        , function/url]
       ['edit'      , "Edit [ctrl+r]" , 'control_stop_blue', view_switcher('edit')]
       ['run'       , "Run [ctrl+r]"  , 'control_play_blue', view_switcher('view')]
       ['hide-bar'  , "Hide Navbar [ctrl+h]"   , 'bullet_arrow_up', navbar_toggle]
       ['embed-link', "Embed"         , 'link'        , show_embed]
       ['screenshot', "Screenshot (for gallery)", 'camera', screenshot]
       'spacer'
       ['undo'      , "Undo"          , 'arrow_undo', (-> pcs.editorUI.undo())]
       ['redo'      , "Redo"          , 'arrow_redo', (-> pcs.editorUI.redo())]
       'spacer'
       ['save'      , "Save [ctrl+s]" , 'page_save'   , editor_save]
       ['rename'    , "Rename"        , 'textfield'   , editor_rename]
       'spacer'
       ['copy'      , "Make Copy"     , 'page_copy'   , editor_copy]
       ['new'       , "New Script"    , 'page_add'    , editor_new]
#       ['rate-up'   , "Awesome"       , 'thumb_up'  , ->]
#       ['rate-down' , "Needs Work"    , 'thumb_down', ->]
       'right'
       ['gallery'   , "Gallery"       , 'images'      , "#gallery/"]
       'spacer'
       ['help'      , "Help"          , 'information' , "#help/"]
       ['coffee-doc', "CoffeeScript Language Reference", 'coffeescript', "http://jashkenas.github.com/coffee-script/#language"]
       ['proc-api'  , "Processing.js API Reference"    , 'processingjs', "http://processingjs.org/reference"]
       'spacer'
       ['login'     , "Login (using google account)"  , 'disconnect'  , "invalid"]
       ['logout'    , "Connected (Click to Logout)"   , 'connect'     , "invalid"]
#       'spacer'
#       ['donate'    , "Donate with Bitcoin"           , 'bitcoin'     , "http://bc.x14.eu/s/1080"],
#       ['flattr'    , "Flattr this"                   , 'flattr', "https://flattr.com/thing/xxxxx/" ]                   
    ]
    
    $(".navbar-show").click(navbar_toggle)
    box = $('#viewNav .buttons-left')
    for b in buttons
        if b == 'right'
            box = $('#viewNav .buttons-right')
            continue
            
        if b == 'spacer'
            box.append("<span class='spacer'></span>")
            continue

        [id, title, icon, action] = b
        
        link = ""
        if typeof(action) == 'string'
            link = "href='#{action}'"
            if action.match(/http:|https:/)
                link += "target='_blank'"
            
        box.append("""
            <a class="button #{id}" title="#{title}" #{link}>
                <span class="sprite s_#{icon}"></span>
            </a>
        """)
        
        if typeof(action) == 'function'
            $("a.button.#{id}").bind('click', action)
            
    navbar_configure([])

    $(".button.login").click((e)->
        # this makes sure the current editor contents are available
        # during the next authentication
        editor_cache()
        NEW_USER_MSG = """
            Please be aware that your scripts will
            be published using the first part of
            of your email address.

            email: john.smith@gmail.com
            name: john.smith
            paths: /john.smith/my_script.coffee
        """
        #if not db.get('mypaths') and not confirm(NEW_USER_MSG)
        #    e.preventDefault()
        #    return false
        return
    )
    auth_setup()
     
    $(document).bind('keydown', (e) ->
        return if not (e.altKey or e.ctrlKey) or (e.ctrlKey and e.which == 65)
        viewName = location.hash[1..4]

        if e.which in [82, 83, 72]
            e.preventDefault()
            e.stopPropagation()

        switch e.which
            when 82
                if viewName == 'edit' then view_switch('view')
                if viewName == 'view' then view_switch('edit')
                return false
            when 83
                editor_save()
                return false
            when 72
                navbar_toggle()
                return false
            when 65
                $('.script-path').select().focus()
                return false
    )
    
