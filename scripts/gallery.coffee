firstOffset = ->
    d = new Date()
    d.setUTCHours(d.getUTCHours() + 2)
    d.setUTCMinutes(0)
    d.setUTCSeconds(0)
    return parseInt(d / SEC)

class GalleryManager

    constructor: ->
        @scripts = []
        @initLayout()

        @pagesize = 6

    initLayout: ->
        g = $("#gallery")
        g.empty()
        $("""<div class='nav-item nav-prev'>
                 <div><span></span></div>
             </div>""").appendTo(g).click(@prev)
        g.append("""<div class="gallery-items"></div>""")
        $("""<div class='nav-item nav-next'>
                 <div><span></span></div>
             </div>""").appendTo(g).click(@next)

    renderItems: ->
        s = @curIndex()
        e = s + @pagesize
        html = []
        for script in @scripts[s...e]
            updated = new Date(script.updated * SEC)

            max_chars = 30 - script.creator.length
            title = path_to_title(script.path).slice(0, max_chars)

            item_top = if script.has_img or not script.summary.trim()
                """
                <a href="/#view#{script.path}">
                    <img class="item-elem default" src="/images#{script.path}"></img>
                </a>
                <a href="/#edit#{script.path}">
                    <div class="item-elem item-text"
                         style="display: none;">
                        <pre>#{script.summary}</pre>
                    </div>
                </a>
                <div class="item-marker"></div>
                """
            else
                """
                <a href="/#view#{script.path}">
                    <img class="item-elem" src="/static/default.png" 
                         style="display: none;"></img>
                </a>
                <a href="/#edit#{script.path}">
                    <div class="item-elem item-text default">
                        <pre>#{script.summary}</pre>
                    </div>
                </a>
                <div class="item-marker" style="left: 128px"></div>
                """
                
            html.push """
                <div class="gallery-item">
                    #{item_top}
                    <div class="item-elem item-bottom">
                        <a href="/#edit#{script.path}" title="edit source">
                            <span class="sprite s_page_edit"></span>
                        </a>
                        <a href="/#view#{script.path}" title="run script">
                            <span class="sprite s_control_play_blue"></span>
                        </a>
                        <span> updated #{prettyDate(updated)} </span>
                        <br/>
                        <span> #{title} by <i>#{script.creator}</i> </span>
                    </div>
                </div>
            """

        $(".gallery-items")
            .empty()
            .append(html.join(""))
        $(".gallery-item")
            .mousemove((e) ->
                txt = $(".item-elem.item-text", this)
                img = $("img.item-elem", this)
                def = $(".item-elem.default", this)
                marker = $(".item-marker", this)

                _y = e.pageY - @offsetTop - 35
                _x = e.pageX - @offsetLeft - 12
 
                in_box = (0 <= _y <= 160) and (0 <= _x <= 256)
                _left = (0 <= _x <= 140)
                show_img = ((_left and in_box ) or
                    (def[0] == img[0] and not in_box) 
                )

                if show_img
                    txt.hide()
                    img.css('display', 'block')
                    marker.css('left', 0)
                else
                    _pos = _x - 200
                    _pos = if in_box and _pos > 0
                        (_pos * _pos) / -10
                    else
                        0
                        
                    txt.find('pre').css('left', _pos + "px")
                    txt.show()
                    img.hide()
                    marker.css('left', "128px")
            )
        p = $(".nav-prev")
        n = $(".nav-next")

        p.addClass("active")
        
        if (e > @scripts.length) and @end_reached
            n.removeClass("active")
        else
            n.addClass("active")

    setCols: (cols) ->
        h = win_dims()[1]
        rows = Math.floor((h - 35) / 245) or 1
        changed = (@pagesize / rows) != cols
        @pagesize = cols * rows 

        if changed then @renderItems()

    curIndex: ->
        for s, i in @scripts
            return i if s.updated <= @offset

        return Math.max(0, @scripts.length - @pagesize)
        
    next: =>
        @setIndex(@curIndex() + @pagesize)

    prev: =>
        @setIndex(Math.max(-1, @curIndex() - @pagesize))

    setIndex: (i=0) ->
        if @end_reached and @scripts.length < i
            i = Math.max(0, @scripts.length - @pagesize)
            o = @scripts[i]?.updated
        else if i == -1 and @newest
            o = @newest
        else
            o = @scripts[i]?.updated
        
        @setOffset(o or 0)

    setOffset: (o)->
        return if o == @offset
        @offset = o
        location.hash = "#gallery/" + o
        
        len = @scripts.length
        load_next = len <= (@curIndex() + @pagesize) and not @end_reached
        if len == 0 or load_next or o < @scripts[0].updated
            load_gallery(@offset, @populate)
        else
            @renderItems()

    populate: (@newest, scripts) =>
        if scripts.length < 20
            @end_reached = true
        
        paths = ender.pluck(scripts, 'path')
        @scripts = ender.reject(@scripts, (s) -> s.path in paths)
        @scripts = @scripts.concat(scripts)
        @scripts.sort((a,b) -> b.updated - a.updated)
        @renderItems()


_gal_man = null
gallery_manager = -> _gal_man or (_gal_man = new GalleryManager())
