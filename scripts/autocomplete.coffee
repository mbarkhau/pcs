isFuncName = (n) -> n.endsWith("\\(\\)")

class Completer

    constructor: (@cm) ->
        @cur = @cm.getCursor(false)
        @tok = @cm.getTokenAt(@cur)
        prev = {line: @cur.line, ch: @tok.start - 1}
        @prevTok = @cm.getTokenAt(prev)

    ###
    # parse for names and documentation
    #
    # may be good place for caching
    ###
    parseScript: ->
        @names = PCS_NAMES
        isGlobal = (n) -> n.isUpper() or isFuncName(n)
        @globals = ender.filter(PCS_NAMES, isGlobal)

    validEvent: (e) ->
        return (
            e.which == 32 and
            e.type == 'keydown' and
            (e.ctrlKey or e.metaKey) and
            not @cm.somethingSelected() and
            @tok.className not in ['comment', 'string']
        )

    atName: -> (
            (@tok.string in @names) or
            (@tok.string + "()" in @names) or
            (@prevTok.string + "()" in @names)
        )

    atPrefix: ->
        s = @tok.string.replace(/^\s+/, '')
        ps = @prevTok.string.replace(/^\s+/, '')
        return s? and ( /^[\w$_]*$/.test(s) or (s == '.' and ps?) )

    getCompletions: ->
        start = @tok.string.replace(/^\s+/, '')
        return @globals if not start?
        return PCS_NAMES if start == '.' and @prevTok.string == 'pcs'
        found = []
        
        return ender.filter(@names, (n) -> n.startsWith(start))

    selected: ->
        if @sel? # complete box
            @sel.find('option:selected').text()
        else if @tok.string == '(' # func call
            @prevTok.string + "()"
        else if @tok.string in @names # other names
            @tok.string
        else
            @tok.string + "()"
    
    selectNext: ->
        opt = @sel.find('option:selected').next()
        if opt.length == 0
            opt = @sel.find('option').first()
        opt.attr('selected', 'selected')

    closeDoc: -> $('.doc-box').empty().remove()

    close: ->
        @closeDoc()
        @box?.empty().remove()
        @doc = @box = @sel = null
        @cm.focus()

    showDoc: =>
        return if not pcs.docs?
        name = @selected()
        @closeDoc()

        doc = pcs.docs[name]
        return if not doc?

        pos = @cm.cursorCoords()
        cur = @cm.getCursor()
        
        docText = ""
        appendDoc = (name, pre=true) ->
            return if not doc[name]?
            docText += "#{name}: <div>"
            if pre
                docText += "<pre>#{doc[name]}</pre>"
            else
                docText += "#{doc[name]}"
            docText += "</div><br/>"

        if doc.Syntax? 
            docText += "<h4>#{doc.Syntax}</h4><br/>"
        else
            docText += "<h4>#{name}</h4><br/>"

        appendDoc('Parameters')
        appendDoc('Returns')
        appendDoc('Description', false)
        appendDoc('Examples')

        @doc = $("<div class='doc-box'>#{docText}</div>")
        tok_pos = {'line': cur.line, 'ch': @tok.start}
        @cm.addWidget(tok_pos, @doc[0], false)

        clearOnMove = =>
            # remove doc on cursor activity
            ncur = @cm.getCursor()
            if @doc? and cur.line == ncur.line and cur.ch == ncur.ch
                setTimeout(clearOnMove, 50)
            else
                @close()

        if @box
            @doc.css('left', pos.x + (@box?.width() or 4) + 'px')
                .css('display', 'block')
        else
            clearOnMove()


    insert: (name=@selected()) ->
        start = if @tok.className then @tok.start else @tok.end
        s_pos = {line: @cur.line, ch: start}
        e_pos = {line: @cur.line, ch: @tok.end}
        if @tok.string == '.'
            @cm.replaceRange(name, e_pos)
        else
            @cm.replaceRange(name, s_pos, e_pos)
        c_pos = s_pos
        c_pos.ch += name.length
        if isFuncName(name)
            c_pos.ch -= 1

        @cm.setCursor(c_pos)
        
    showCompletions: (completions, e) ->
        if completions.length == 1
            # complete directly if only one result
            # only trigger with event (see @sel.keydown handler)
            @insert(completions[0])
            return

        # build widget
        @box = $("<div class='completions'></div>")
        @sel = $('<select/>')
            .attr('size', Math.min(10, completions.length))
            .attr('multiple', false)
            .appendTo(@box)

        cur = @cm.getCursor()
        start = if @tok.className then @tok.start else @tok.end
        tok_pos = {'line': cur.line, 'ch': start}
        @cm.addWidget(tok_pos, @box[0], false)

        for c in completions
            $("<option>#{c}</option>").appendTo(@sel)
            
        @selectNext()
        done = false

        pick = (e) =>
            e?.stopPropagation()
            @insert(@selected() or completions[0])
            @close()

        @sel.dblclick(pick)
            .keydown((e) =>
                stop = -> e.preventDefault()
                key = e.which
                return if key in [16,17]
                if key == 32 # space
                    @selectNext()
                else if key == 13 # enter
                    stop()
                    pick()
                else if key == 27 # esc
                    @close()
                else if key not in [34, 33, 38, 40] # not up, down
                    @close()
                    setTimeout((=> onComplete(@cm)), 50)
            ).change(@showDoc)

        clearOnMove = =>
            # remove doc on cursor activity
            ncur = @cm.getCursor()
            if cur.line == ncur.line and cur.ch == ncur.ch
                setTimeout(clearOnMove, 50)
            else
                done = true
                @close()

        clearOnMove()
        @sel.focus()


doComplete = (cm, c, e) ->
    c.parseScript()

    #print "atname", c.atName()
    #print "atprefix", c.atPrefix()

    if c.atName()
        c.showDoc()
        return
    else if c.atPrefix()
        comps = c.getCompletions()
        return if comps.length == 0

        c.showCompletions(comps, e)


onComplete = (cm, e) ->
    if not pcs.docs
        load_docs()

    c = new Completer(cm)
    if e?
        if e.which == 27
            c.close()
        return if not c.validEvent(e)
        e.preventDefault()

    load_deps(cur_path(), -> doComplete(cm, c, e))
