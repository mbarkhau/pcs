###
keeps editor at proper height
###
resize = ->
    [w, h] = win_dims()
    log_h = logger_panel()?.height() or 0
    if log_h > 0
        log_h += 6
    nav_h = 24 #$('#viewNav').height()
    $('#gallery, #editor').width(w)
    $('#gallery, #editor, #help').height(h-(log_h + nav_h))

    gallery_widths = [320, 620, 920, 1220, 1520, 1820, Infinity]
    for gal_w, cols in gallery_widths
        break if gallery_widths[cols + 1] + 60 > w
        
    gallery_manager().setCols(cols+1)
    $('#gallery .gallery-items').width(gal_w)

    setTimeout(resize, 250)

###
hides all panels but the specified one
###
show_panel = (panel) ->
    nodes = {
        'editor' : $('#editor')
        'viewer' : $('#canvas')
        'gallery': $('#gallery')
        'help'   : $('#help')
    }

    for name, node of nodes
        node.css('display', 'none')
    
    nodes[panel]?.css('display', 'block')

    close_log()

    
view_editor = ->
    script = cur_script()
    console.log "edit script try ", script
    if not script
        return load_script(cur_path(), view_editor)

    if not CodeMirror?
        #timeout until lib is ready
        return setTimeout(view_editor, 20)

    #console.log "edit script success ", script
    btn = if pcs.username and pcs.username == script.creator
        ['run', 'undo', 'redo', 'save', 'rename', 'copy', 'new']
    else if pcs.username
        ['run', 'undo', 'redo', 'save', 'new']
    else
        (['run', 'undo', 'redo'])

    navbar_configure(btn)
    navbar_show()

    runtime_halt()
    show_panel('editor')
    update_path()
    editor_load()

view_script = ->
    script = cur_script()
    if not script
        return load_script(cur_path(), view_script)

    if not (Processing? and CoffeeScript?)
        return setTimeout(view_script, 20)

    btn = if pcs.username and pcs.username == script.creator
        ['edit', 'hide-bar', 'embed-link', 'screenshot']
    else
        ['edit', 'hide-bar', 'embed-link']

    navbar_configure(btn)
    navbar_show(db.get('navbar'))

    err = error_check()
    if err # don't switch when there are compile errors
        view_switch('edit')
    else
        show_panel('viewer')
        runtime_halt()
        runtime_start()

    update_path()

embed_script = ->
    load_lib()

    navbar_show(false)
    script = cur_script()
    if not script
        return load_script(cur_path(), embed_script)

    if not (Processing? and CoffeeScript?)
        return setTimeout(embed_script, 20)

    runtime_halt()
    runtime_start()
    show_panel('viewer')

    update_path()

show_embed = ->
    confirm("""
        <iframe src="http://pcsedit.appspot.com/#embed#{cur_path()}"
                frameborder="0" scrolling="no"
                style="width:600px;height:400px;">
        </iframe>
    """)

view_gallery = ->
    p = @params.page
    if not p
        o = firstOffset()
        location.hash = "#gallery/" + o
        $(".button.gallery").attr("href", "/#gallery/" + o)
        return

    if pcs.username
        navbar_configure(['new'])
    else
        navbar_configure([])
        
    update_path("/")
    runtime_halt()
    show_panel('gallery')

    m = gallery_manager()
    m.setOffset(parseInt(p))

view_help = ->
    navbar_configure([])
    update_path("/")

    runtime_halt()
    show_panel('help')

view_switch = (viewName, path) ->
    console.log "switch init", path
    l = location
    if l.hash.startsWith("#edit")
        editor_halt()
    return

    l.hash = if path?
        "#" + viewName + path
    else
        l.hash.replace(/#\w+/, '#' + viewName)

    console.log "switched to", path

view_switcher = (viewName, path) -> ( -> view_switch(viewName, path) )

loc_parts    = -> window.location.hash.split("/")
cur_path     = -> "/" + loc_parts()[1..].join("/")
cur_subpath  = -> "/" + loc_parts()[2..].join("/")
cur_username = -> "/" + loc_parts()[1]

init_views = ->
    resize() # start loop    
    init_suggest()

    show_panel() # no argument -> hide all

    clear_cache()
    navbar_init()
    
    window.onerror = log
    pathjs = ender.pathjs

    m = pathjs.map
    m("#edit/:user/:path").to(view_editor)
    m("#view/:user/:path").to(view_script)
    m("#embed/:user/:path").to(embed_script)
    m("#gallery/:page").to(view_gallery)
    m("#gallery/").to(view_gallery)
    m("#help/").to(view_help)
    
    initial_path = db.get('lastpath') or DEFAULT_PATH
    pathjs.root("#edit" + initial_path)
    pathjs.rescue(view_switcher('gallery'))
    pathjs.listen()

setTimeout(init_views, 100)

