$ = jQuery
window.pcs = {}

EMAIL_RE = /^([A-Za-z0-9_\-\.]+)\@([A-Za-z0-9_\-\.]+)\.([A-Za-z]{2,4})$/

DEFAULT_PATH = if location.host.indexOf('pcsedit') > -1
    "/mbarkhau/example.coffee"
else
    "/test/example.coffee"

SEC  = 1000
MIN  = 60 * SEC
HOUR = 60 * MIN
DAY  = 24 * HOUR
WEEK = 7  * DAY
YEAR = 365.25 * DAY


PCS_USER_FUNCS = [
    'setup'
    'draw'

    'mouseMoved'
    'mouseDragged'
    'mouseClicked'
    'mousePressed'
    'mouseOut'
    'mouseOver'
    'mouseReleased'

    'keyPressed'
    'keyReleased'
    'keyTyped'
]

PCS_NAMES = [
    "Array",             "ArrayList",       "HALF_PI",
    "HashMap",           "Infinity",        "NaN",
    "Object",            "PFont",           "PGraphics",
    "PI",                "PImage",          "PShape",
    "PVector",           "QUARTER_PI",      "String",
    "TWO_PI",            "XMLElement",      "abs()",
    "acos()",            "alpha()",         "ambient()",
    "ambientLight()",    "and",             "append()",
    "applyMatrix()",     "arc()",           "arguments",
    "arrayCopy()",       "asin()",          "atan()",
    "atan2()",           "background()",    "beginCamera()",
    "beginShape()",      "bezier()",        "bezierDetail()",
    "bezierPoint()",     "bezierTangent()", "bezierVertex()",
    "blend()",           "blendColor()",    "blue()",
    "box()",             "break",           "brightness()",
    "camera()",          "catch",           "ceil()",
    "class",             "color()",         "colorMode()",
    "concat()",          "constrain()",     "continue",
    "copy()",            "cos()",           "createFont()",
    "createGraphics()",  "createImage()",   "cursor()",
    "curve()",           "curveDetail()",   "curvePoint()",
    "curveTangent()",    "curveTightness()","curveVertex()",
    "day()",             "degrees()",       "directionalLight()",
    "dist()",            "do",              "draw()",
    "ellipse()",         "ellipseMode()",   "else",
    "emissive()",        "endCamera()",     "endShape()",
    "eval",              "exit()",          "exp()",
    "expand()",          "extends",         "false",
    "fill()",            "filter()",        "finally",
    "floor()",           "focused",         "for",
    "frameCount",        "frameRate",       "frameRate()",
    "frustum()",         "get()",           "green()",
    "height",            "hex()",           "hint()",
    "hour()",            "hue()",           "if",
    "image()",           "imageMode()",     "in",
    "join()",            "key",             "keyCode",
    "keyPressed",        "keyPressed()",    "keyReleased()",
    "keyTyped()",        "lerp()",          "lerpColor()",
    "lightFalloff()",    "lightSpecular()", "lights()",
    "line()",            "link()",          "loadBytes()",
    "loadFont()",        "loadImage()",     "loadPixels()",
    "loadShape()",       "loadStrings()",   "log()",
    "loop()",            "mag()",           "map()",
    "match()",           "matchAll()",      "max()",
    "millis()",          "min()",           "minute()",
    "modelX()",          "modelY()",        "modelZ()",
    "month()",           "mouseButton",     "mouseClicked()",
    "mouseDragged()",    "mouseMoved()",    "mouseOut()",
    "mouseOver()",       "mousePressed",    "mousePressed()",
    "mouseReleased()",   "mouseX",          "mouseY",
    "new",               "nf()",            "nfc()",
    "nfp()",             "nfs()",           "noCursor()",
    "noFill()",          "noLights()",      "noLoop()",
    "noSmooth()",        "noStroke()",      "noTint()",
    "noise()",           "noiseDetail()",   "noiseSeed()",
    "norm()",            "normal()",        "not",
    "null",              "of",              "off",
    "on",                "online",          "or",
    "ortho()",           "perspective()",   "pcs",
    "pmouseX",           "pmouseY",         "point()",
    "pointLight()",      "popMatrix()",     "popStyle()",
    "pow()",             "print()",         "printCamera()",
    "printMatrix()",     "printProjection()", "println()",
    "pushMatrix()",      "quad()",          "radians()",
    "random()",          "randomSeed()",    "rect()",
    "rectMode()",        "red()",           "redraw()",
    "requestImage()",    "resetMatrix()",   "return",
    "reverse()",         "rotate()",        "rotateX()",
    "rotateY()",         "rotateZ()",       "round()",
    "saturation()",      "save()",          "saveFrame()",
    "saveStrings()",     "scale()",         "screen",
    "screenX()",         "screenY()",       "screenZ()",
    "second()",          "set()",           "setup()",
    "shape()",           "shapeMode()",     "shininess()",
    "shorten()",         "sin()",           "size()",
    "smooth()",          "sort()",          "specular()",
    "sphere()",          "sphereDetail()",  "splice()",
    "split()",           "splitTokens()",   "spotLight()",
    "sq()",              "sqrt()",          "status()",
    "str()",             "stroke()",        "strokeCap()",
    "strokeJoin()",      "strokeWeight()",  "subset()",
    "super",             "switch",          "tan()",
    "text()",            "textAlign()",     "textAscent()",
    "textDescent()",     "textFont()",      "textLeading()",
    "textMode()",        "textSize()",      "textWidth()",
    "texture()",         "textureMode()",   "then",
    "throw",             "tint()",          "translate()",
    "triangle()",        "trim()",          "true",
    "try",               "unbinary()",      "undefined",
    "unhex()",           "unless",          "until",
    "updatePixels()",    "vertex()",        "when",
    "while",             "width",           "year()"
]
