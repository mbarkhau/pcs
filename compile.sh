#!/usr/bin/env bash
cd ~/workspace/pcs/
echo "compiling individual scripts"
coffee -o static -c scripts/*.coffee
echo "bundling scripts "
coffee -cj lib/pcs.js scripts/*.coffee
echo "compressing scripts "
uglifyjs -o lib/pcs.min.js lib/pcs.js
#cd ~/workspace/jslib/
#ender compile ../pcs/lib/jquery-ui.min.js
#echo "copying ender-app.js to lib/"
#cp ender-app.js ../pcs/lib/
