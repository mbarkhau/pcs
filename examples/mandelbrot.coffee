msaa_level = 2 # Mutisample Anti-aliasing :)

trans = 1 / msaa_level / 2

zoom = [
    -2.5  # x
    1.3 # y
    4   # w
    4   # h
]

class ComplexNumber
    constructor: (@r, @i) ->

    add: (c) -> new ComplexNumber(@r + c.r, @i + c.i)
    sub: (c) -> new ComplexNumber(@r - c.r, @i - c.i)
    mult: (c) -> new ComplexNumber(@r*c.r - @i*c.i, @r*c.i + @i*c.r)
    modSquared: -> @r*@r + @i*@i

pixelToPoint = (x, y, w, h) ->
    stepx = zoom[2] / w
    stepy = zoom[3] / h
    return new ComplexNumber(zoom[0]+x*stepx, zoom[1]-y*stepy)

zero4negative = -> max(0, v)

z_val = (x, y) ->
    pix = 0
    for _y in [0...msaa_level]
        for _x in [0...msaa_level]
            z = new ComplexNumber(0,0)
            c = pixelToPoint(
                zero4negative(h-w)/2+x+(_x/msaa_level)+trans,
                zero4negative(w-h)/2+y+(_y/msaa_level)+trans,
                max(w,h),
                max(w,h)
            )

            i = 0
            while z.modSquared() < 4
                z = z.mult(z).add(c)
                i++
                if i > 63
                    # i = 0
                    break

            pix += i

    return pix


res = 64

w = pcs.width
h = pcs.height

y = 0


setup = ->
    noStroke()
    
draw = ->
    if y > h and res == 1
        noLoop()
        return
    
    if y > h
        res = max(1, floor(res / 4))
        y = 0
    
    res_half = res / 2
    for x in [0...w] by res
        pix = floor(z_val(x, y))
        fill((pix*4/(msaa_level*msaa_level))-1)
        rect(x-res_half, y-res_half, 2*res, 2*res)

    y += res

