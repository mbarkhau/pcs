###
# Precipice by Andor Salga
###
TEXT = "Gears of Flesh turn & tumble..."


draw = ->
    background(0)
    key = 0
    x = pcs.mouseX
    y = pcs.mouseY


    for i in [0...30]

        xPos = (i * 15) + pcs.width/4
        yPos = (pcs.height/10 + sin(i/4.0)*100.0 + key)

        fill(
            max( mag(xPos - x, xPos - y ),0),
            max( mag(xPos - x, yPos - x ),60),
            max( mag(xPos - x, yPos - y ),2),
            max( 230 - mag(xPos - x, yPos - y ),0)
        )

        key += 15

        pushMatrix()
        translate(xPos, yPos)

        rotate((mag(xPos - x, yPos-y)/100.0)*
               (mag(xPos - x, 0)/100.0))

        scale(key/1000.0 + mag(xPos - x, yPos - y)/10.0 )

        if x == 0 and i == 1
            text("?",1 ,1 )
        else
            text(TEXT.charAt(i), 1, 1)

        popMatrix()

