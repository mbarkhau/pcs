
prev_slide = 0
cur_slide = 0
zoom = 1

base_size = 70

fade_progress = 0
fade_time = 5 # seconds

fade_dir = 0

switch_slide = (s) ->
    if cur_slide != s
        fade_dir = 1
        
    prev_slide = cur_slide
    cur_slide = s


next = ->
    switch_slide(cur_slide + 1)

prev = ->
    switch_slide(cur_slide-1, 0)

first = ->
    switch_slide(0)

last = ->
    switch_slide(99999)

pcs.mouseClicked = ->
    if pcs.mouseX < pcs.width / 2
    	prev()
    else
    	next()


keyHandler = ->
    k = pcs.keyCode
    if k in [33, 37, 38, 75, 105, 104, 100]
    	prev()
    if k in [10, 32, 34, 39, 40, 74, 102, 99, 98]
        next()
    if k in [36, 103]
    	first()
    if k in [35, 97]
    	last()
    
    if k == 109
        zoom = max(0.4, zoom - 0.3)
    if k == 107
        zoom = min(4.5, zoom + 0.3)
    if k == 96
        zoom = 1
    
    textSize(base_size * zoom)
    #println k

    
jQuery('canvas').focus()

goog = "http://www.google.de/logos/"

bound = (x, low, top) -> max(min(x, top), low)

drawSlides = (slides, opts) ->
    
    w = pcs.width
    h = pcs.height
    base_size = h / 10
    
    textSize(base_size * zoom)
    textAlign(pcs.CENTER)
    
    pcs.keyPressed = keyHandler
    pcs.frameRate(60)
    fade_delta = (60 / fade_time) *  2.56
    
    pcs.draw = ->
        cur_slide = bound(cur_slide, 0, slides.length - 1)
        slide = slides[if fade_dir > 0 then prev_slide else cur_slide]

        background(0)
        fill(255)
        
        if typeof slide == 'string'
            text(slide, w/2, h/3)
        else # assumed to be an image
            iw = slide.width * zoom
            ih = slide.height * zoom
            image(slide, w/2 - iw/2, h/3 - ih/2, iw, ih)

        if fade_progress > 0
            fill(0, fade_progress)
            rect(0, 0, w, h)

        fade_progress += fade_delta * fade_dir
        if fade_progress > 255
            fade_dir = -1
        if fade_progress < 0
            fade_dir = 0


exports = { drawSlides }


test_slides = [
    "first slide"
    "second slide"
    "third slide"
    "fourth slide"
    loadImage(goog + "charlesdarwin_09.gif")
    loadImage(goog + "zuse10-hp.gif")
    "seventh slide"
    "last slide"
]


drawSlides(test_slides)
