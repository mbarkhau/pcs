fib = (n) ->
    if n in [0, 1]
        n
    else
        fib(n-1) + fib(n-2)

###
# Illustration branching in fibonacci sequence
# distance of n from left is fib(n) * 2
#                                                                    9
#                                          8
#                          7                               6         
#                6                   5               5           4   
#          5           4         4       3       4       3     3   2 
#      4       3     3   2     3   2   2  1    3   2   2  1  2  1 1 0
#    3   2   2  1  2  1 1 0  2  1 1 0 1 0    2  1 1 0 1 0   1 0      
#  2  1 1 0 1 0   1 0       1 0             1 0
# 1 0
# 
# http://www.youtube.com/watch?v=kkGeOWYOFoA
###

fib = ender.memoize(fib)
