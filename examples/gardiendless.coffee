###
# gardiendless v6 by Michael Schieben
###

setup = ->
    smooth()
    background(0)

draw = ->
    tick = pcs.frameCount
    w = pcs.width
    h = pcs.height

    translate(w/2, h/2)
    #translate(pcs.mouseX, pcs.mouseY)
    scale(0.5)

    pushMatrix()
    rotate(radians(tick % 720))

    stroke(255)

    if tick % w > tick % h
        line(tick % w, 0 - h, tick % w, h * 2)
    else
        line(0 - w, tick % h, w * 2, tick % h)

    popMatrix()

    rotate(radians(-tick % 360))

    stroke(0)
    if (w - tick % w) > (h - tick % h)
        line((w - tick % w), 0 - h, (w - tick % w), h * 2)
    else
        line(0 - w, (h - tick % h), w * 2, (h - tick % h))
