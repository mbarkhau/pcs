###
# (C) JOAO MARTINHO MOURA
#
# www.jmartinho.net
#
# HTML5 Visual Art - " Close Line, 2011 "
#
# Close Line: infinite abstract visual b&w line work,
# by artist João Martinho Moura, 2011.
###

pointCount = 200

XPOS = YPOS = RND_XPOS = RND_YPOS = null

MX = pcs.width / 2
MY = pcs.height / 2

mouseIdx = 0

randint = (bound) -> round(random(-bound, bound))

setup = ->
    frameRate(25)
    background(0xff)

    fill(0xff, 50)
    noStroke()
    
    initArr = (v) -> v for i in [0...pointCount]
    
    XPOS     = initArr(MX)
    YPOS     = initArr(MY)
    RND_XPOS = initArr(MX)
    RND_YPOS = initArr(MY)

draw = ->
    rect(0, 0, pcs.width, pcs.height)
    
    mouseIdx += 1
    if mouseIdx > pointCount
        mouseIdx = 0
    
    XPOS[mouseIdx] = pcs.mouseX
    YPOS[mouseIdx] = pcs.mouseY
    RND_XPOS[mouseIdx] = pcs.mouseX + randint(10)
    RND_YPOS[mouseIdx] = pcs.mouseY + randint(10)
        
    for i in [0...pointCount]
        if i > 0
            x_a = RND_XPOS[i-1]
            y_a = RND_YPOS[i-1]
            x_b = XPOS[i-1]
        else
            x_a = MY
            y_a = YPOS[i-1]
            x_b = MY
            
        stroke(0x22, 20)
        line(RND_XPOS[i], RND_YPOS[i], x_a, y_a)
        stroke(0x22, 80)
        line(XPOS[i], YPOS[i], x_b, YPOS[i-1])

        XPOS[i] = XPOS[i] + randint(3)
        YPOS[i] = YPOS[i] + randint(3)
        RND_XPOS[i] = RND_XPOS[i] + randint(4)
        RND_YPOS[i] = RND_YPOS[i] + randint(4)


