###
# Nautilus by Andor Salga
# adapted by Manuel Barkhau
###

setup = () ->
    background(0)
    frameRate(60)
    stroke(255, 0, 0, 10)

draw = () ->
    frame = pcs.frameCount
    
    for i in [1...100]
        strokeWeight(6)
        pushMatrix()
        translate(pcs.width/2, pcs.height/2)
        rotate((i*50) + frame/100/ (i*2))
        translate(
            cos((i*60) + frame/50 ) * (i + 10),
            sin((i*50) + frame/80) * ((i*8) + 30)
        )
        point(i, i)
        popMatrix()
    
