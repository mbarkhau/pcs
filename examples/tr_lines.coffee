###
# (C) JOAO MARTINHO MOURA, 2011
#
# www.jmartinho.net
#
# HTML5 Visual Art - " Transversal Lines, 2011 "
#
# Transversal Lines: infinite abstract visual b&w line work,
# by artist João Martinho Moura, 2011.
###

tickRefresh = 0
tickBW = 0
tickClear = 0
tick = 0

pointCount = 30

XPOS = YPOS = RND_XPOS = RND_YPOS = null

w = pcs.width
h = pcs.height

MX = w / 2
MY = h / 2

mouseIdx = 0

randint = (bound) -> round(random(-bound, bound))
boundArrVal = (arr, i, max) ->
    return if 0 <= arr[i] < max
    arr[i] = random(0, max)

setup = ->
    frameRate(25)
    background(0xff)

    initArr = (v) -> v for i in [0...pointCount]
    
    XPOS     = initArr(MX)
    YPOS     = initArr(MY)
    RND_XPOS = initArr(MX)
    RND_YPOS = initArr(MY)

mousePressed = ->
    MX = pcs.mouseX
    MY = pcs.mouseY

draw = ->
    tickClear += 1
    if tickClear > 2000
        fill(255, 255, 255, 40)
        rect(-50, -50, w+50, h+50)
        tickClear = 0

    tick += 1

    if tick > 15
        MX = random(50, w-50)
        MY = random(50, pcs.height-50)
        tick = 0
    
    mouseIdx += 1

    if mouseIdx < pointCount
        if 40 < MX < w-40
            XPOS[mouseIdx] = MX
            RND_XPOS[mouseIdx] = MX + randint(10)
        
        if 40 < MY < pcs.height-40
            YPOS[mouseIdx] = MY
            RND_YPOS[mouseIdx] = MY + randint(10)
        
    else
        mouseIdx = 0
    
    tickBW += 1

    if tickBW > 400
        tickBW = 0
    
    if tickClear in [50, 400]
        strokeWeight(10)
    else
        strokeWeight(1)
    
    for i in [0...pointCount]
        stroke(10, 10, 10, 20)
        line(RND_XPOS[i], RND_YPOS[i], RND_XPOS[i-1], RND_YPOS[i-1])

        if tickBW < 200
            stroke(10, 10, 10, 80)
        else
            stroke(255, 255, 255, 80)
        
        line(XPOS[i], YPOS[i], XPOS[i-1], YPOS[i-1])

        XPOS[i] = XPOS[i] + randint(3)
        YPOS[i] = YPOS[i] + randint(3)
        RND_XPOS[i] = RND_XPOS[i] + randint(5)
        RND_YPOS[i] = RND_YPOS[i] + randint(5)

        boundArrVal(XPOS, i, w)
        boundArrVal(YPOS, i, h)
        boundArrVal(RND_XPOS, i, w)
        boundArrVal(RND_YPOS, i, h)

    tickRefresh += 1

    if tickRefresh > 1600
        background(0xff)
        tickRefresh = 0


