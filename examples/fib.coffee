
X = Y = 0

setup = ->
  noFill()

  X = @width  / 2
  Y = @height / 2


a = 0
b = 1

draw = ->
  [a, b] = [b, a + b] # fib sequence
  rect(X, Y, a, a)

  switch @frameCount % 4
    when 0
      X = X + a
      Y = Y - (b - a)
    when 1
      X = X - (b - a)
      Y = Y - b
    when 2
      X = X - b
      Y = Y
    when 3
      X = X
      Y = Y + a
    
  exit() if b > @width*2

