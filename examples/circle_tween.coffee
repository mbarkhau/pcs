###
# Circle Tween by Andor Salga
###

class Particle

    constructor: () ->
        @startPos = new PVector()
        @endPos = new PVector()
        @currPos = new PVector()
        @red = 255
    
    update: (normalizedValue) ->
        @currPos.x = @startPos.x + (@endPos.x * normalizedValue)
        @currPos.y = @startPos.y + (@endPos.y * normalizedValue)
        @red = 255.0 * normalizedValue
    
    draw: () ->
        strokeWeight(2)
        stroke(@red, 255-@red, 0)
        point(@currPos.x, @currPos.y)


points = []
w = h = 0

setup = ->
    background(0)
    w = pcs.width
    h = pcs.height
    
    for i in [0...PI*2] by 0.01
        p = new Particle()
        p.startPos = (new PVector(w/2 + cos(i) * 50.0, h/2 + sin(i) * 50.0))
        p.endPos = (new PVector(random(-w/2, w/2), random(-h/2, h/2)))
        points.push(p)


draw = ->
    noStroke()
    fill(0, 10)
    rect(0, 0, w, h)
    
    for p in points
        p.update(pcs.mouseX/w)
        p.draw()
    

