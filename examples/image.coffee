
goog = "http://www.google.de/logos/"

IMAGES = [
    loadImage(goog + "2011/jorge_luis_borges-2011-hp.jpg")
    loadImage(goog + "2011/goethe-2011-hp.jpg")
    loadImage(goog + "zuse10-hp.gif")
    loadImage(goog + "charlesdarwin_09.gif")
]

draw = ->
    y = 30
    for img, i in IMAGES
        image(img, 0, y, img.width, img.height)
        y += img.height

