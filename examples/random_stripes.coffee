i = 0

setup = ->
    smooth()
    strokeWeight(10)

draw = ->
    stroke(random(50), random(255), random(255), 100)
    line(i, 0, random(0, pcs.width), pcs.height)
    if i > pcs.width
        i = 0

    i += 1
