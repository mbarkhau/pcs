###
# Crazy Delicious by David Guttman
#
# https://github.com/davidguttman/crazy_delicious_coffee_processing
###

class Bean

    constructor: (@x, @y, @x_off, @y_off, @vel=3, @accel=-0.003) ->
    
    draw: ->
        @x_off += 0.0007
        @y_off += 0.0007

        @vel += @accel

        @x += pcs.noise(@x_off) * @vel - @vel/2
        @y += pcs.noise(@y_off) * @vel - @vel/2

        @set_color()
        pcs.point(@x, @y)

    set_color: ->
        hue = pcs.noise((@x_off+@y_off)/2)*360

        pcs.colorMode(pcs.HSB, 360, 100, 100)
        pcs.stroke(hue, 100, 200, 4)


beanbag = []

addBean = ->
    x_off = pcs.frameCount * 0.0003
    y_off = x_off + 20
    
    x = pcs.noise(x_off) * pcs.width
    y = pcs.noise(y_off) * pcs.height
    
    beanbag.push(new Bean(x, y, x_off, y_off))


setup = ->
    background(0)
    frameRate(300)

draw = ->
    if @frameCount % 8 == 0
        addBean()
        beanbag = ender.reject(beanbag, (b) -> b.vel <= 0)

    for bean in beanbag
        bean.draw()


