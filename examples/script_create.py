from backend.models import Script

path = "/testss/foo.coffee"
content = """
###
# Tickle, written by 
#  Casey Reas: http://reas.com/
#  Ben Fry   : http://benfry.com/
###

x = width  / 2
y = height / 2

draw = () ->
  fill(204, 120)
  rect(0, 0, width, height)
  fill(0)
  
  deltaX = abs(pjs.mouseX - (x + 10))
  deltaY = abs(pjs.mouseY - (y - 5))
  
  # If the cursor is over the text, change the position
  if (deltaX < 25) and (deltaY < 10)
    x += random(-5, 5)
    y += random(-5, 5)
  
  text("tickle", x, y)

"""

s = Script(key_name=path, path=path, creator="testss", public=True, content=content)

s.put()
