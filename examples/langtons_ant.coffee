###
# Langton's Ant demos, inspired by Aldo Cavini Benedetti
#
# http://www.youtube.com/watch?v=1X-gtr4pEBU
###
colors = [
    color(255)
    color(255, 0, 0)
    color(0, 255, 0)
    color(0, 0, 255)
    color(255, 0, 255)
    color(255, 255, 0)
    color(155, 0, 0)
    color(0, 155, 0)
    color(0, 0, 155)
    color(155, 0, 155)
    color(155, 155, 0)
    color(255, 50, 50)
    color(50, 255, 50)
    color(50, 50, 255)
    color(255, 50, 255)
    color(255, 255, 50)
]

###
# Create a step function.
# When invoked the ant will take another step.
# 
# make_stepper(antspec, gridwidth, 
# example:
#    make_stepper(['right', 'left']) # the canonical stepper
#    make_stepper(['right', 'left', 'left']) # fast highway
#    make_stepper(['right', 'right', 'left']) # chaotic
###
make_stepper = (spec=['right','left'], s=300)->
    background(100)
    noStroke()
    specsize = spec.length
        
    cw = ceil(min(pcs.width/s, pcs.height/s)) # cell width in pixels
    
    x = floor(s/2)
    y = floor(s/2)
    
    grid = for _x in [0...s*s] then 0
    
    dir = 0 # up
    
    stepfn = ->
        return if not (0 < x < s and 0 < y < s)

        pos = x + y*s
        i = (grid[pos] + 1) % specsize
        fill(colors[i])
        grid[pos] = i
        
        if spec[i] == 'right'
            dir = (dir + 1) % 4
        else
            dir = (dir + 3) % 4
        
        rect(x*cw, y*cw, cw, cw)
        
        if dir == 0
            y -= 1 # up
        else if dir == 1
            x += 1 # right
        else if dir == 2
            y += 1 # down
        else
            x -= 1 # left
        
    return stepfn

make_drawer = (antspec, speed, grid=floor(pcs.height/2)) ->
    return ->
        step = make_stepper(antspec, grid)
        pcs.draw = -> for i in [0..speed] then step()

drawers = [
    make_drawer(['right', 'left'], 100),
    make_drawer(['right', 'left', 'left'], 20), # fast highway
    make_drawer(['right', 'right', 'left'], 1000), # chaotic
    make_drawer(['right', 'left', 'left', 'right'], 1000), # tumor
    make_drawer(['right', 'right', 'left', 'left'], 1000), # castle
    make_drawer([
        'right', 'right', 'left', 'right', 'left', 'right',
        'left','left','right', 'left'
    ], 20),

    make_drawer([ # expanding dock
        'right', 'right', 'right', 'left', 'right', 'left',
        'left', 'right', 'left','right'
    ], 1500)

    make_drawer([ # horizontal bar
        'right', 'right', 'right', 'left', 'left', 'left',
        'right', 'right', 'right', 'left', 'right', 'right'
    ], 5000),

    make_drawer([ # snaker
        'right', 'left', 'left', 'right', 'right',  'right',
        'left', 'right', 'left', 'right', 'left', 'left'
    ], 400),
    
    make_drawer([ # triangle filler 
        'right', 'right', 'right', 'left', 'right',  'left',
        'right'
    ], 400),
    
    make_drawer([ # pyramid filler 
        'right', 'right', 'right', 'left', 'left',  'left',
        'right', 'left', 'left',  'left', 'right', 'right',
    ], 400),
    
    make_drawer([ # right angled triangle
        'right', 'right', 'right', 'left', 'left',  'left',
        'right', 'right', 'right', 'right', 'right',  'left',
    ], 1000),
    
    make_drawer([ # glider
        'right', 'right', 'right', 'left', 'right',  'left',
        'left', 'right', 'right', 'right', 'right', 'right',
    ], 1000),
    
    make_drawer([ # square fill
        'right', 'right', 'right', 'left', 'right'
    ], 1000),
    
    make_drawer([ # bounce in square
        'right',  'left', 'right', 'right', 'right', 'right',
        'right', 'left', 'left',
    ], 1000),
    
    make_drawer([ # spiral bouncer
        'right',  'left', 'right', 'right', 'right', 'right',
        'left', 'left', 'left', 'right', 'right',
    ], 1000),
]

n = -1

stroke(0)
textAlign(CENTER)

textSize(60)
text("Click!", pcs.width/2, pcs.height/3)
textSize(50)
text("(cycle through ant configurations)", pcs.width/2, pcs.height/2)

mousePressed = ->
    len = drawers.length
    
    n = if pcs.mouseButton == 37
    	(n + 1) % len
    else
        (n + len - 1) % len
        
    drawers[n]()



