###
# Adapted from example in PomaxGuide
# http://processingjs.org/reference/articles/PomaxGuide
###

value = 0
w = pcs.width
h = pcs.height

setup = ->
    noLoop()

draw = ->
    # partially clear, by overlaying a semi-transparent rect  
    # with background color  
    noStroke()
    fill(255, 75)
    rect(0, 0, w, h)
    # draw the "sine wave"  
    stroke(100, 100, 200)
    noFill()
    
    #bezier(0,100, 33,100+value, 66,100+value, 100,100)
    bezier(0, h/2, w/6, (h/2) + value, w*(2/6),(h/2) + value, w/2, h/2)
    bezier(w/2, h/2, w*4/6, h/2 -value, w*5/6, h/2 - value, w, h/2)

mouseMoved = ->
    value = pcs.mouseY - h/2
    redraw()

