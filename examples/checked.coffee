setup = ->
    frameRate(10)
    background(127)
    noStroke()

draw = ->
    i = pcs.frameCount * 20
    fill(0)
    rect(0, i, pcs.width, 10)
    fill(255)
    rect(i, 0, 10, pcs.height)

