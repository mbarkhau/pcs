###
# Swarm by Andor Salga
###

setup = () ->
  background(0)
  
draw = () ->
  noStroke()
  fill(0, 5)
  frame = pcs.frameCount
  
  rect(0, 0, width, height)
  noFill()
  stroke(255, 0, 0, 10)
  for i in [1...50]
    strokeWeight(5.0)
    pushMatrix()
    translate(width/2.0, height/2.0)
    rotate((i*50) + frame/100.0/ (i*2))
    translate(
      cos((i*60) + frame/50.0 ) * (i + 10.0),
      sin((i*50) + frame/80.0) * ((i*1) + 30.0)
    )
    point(i, i)
    popMatrix()
