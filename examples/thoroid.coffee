###
# Interactive Toroid
# by Ira Greenberg.
#
# Illustrates the geometric relationship between Toroid, Sphere, and Helix
# 3D primitives, as well as lathing principal.
#
# Instructions:
# UP arrow key pts++
# DOWN arrow key pts--
# LEFT arrow key segments--
# RIGHT arrow key segments++
# 'a' key toroid radius--
# 's' key toroid radius++
# 'z' key initial polygon radius--
# 'x' key initial polygon radius++
# 'w' key toggle wireframe/solid shading
# 'h' key toggle sphere/helix
###

pts = 40 # int
angle = 0.0
radius = 60.0

# lathe segments
segments = 60 # int
latheAngle = 0.0
latheRadius = 100.0

# vertices
vertices = []
vertices2 = []

# for shaded or wireframe rendering
isWireFrame = false

# for optional helix
isHelix = false
helixOffset = 5.0

setup = -> size(640, 360, P3D)

draw = ->
    background(50, 64, 42)

    # basic lighting setup
    lights()

    #  2 rendering styles
    #  wireframe or solid
    if isWireFrame
        stroke(255, 255, 150)
        noFill()
    else
        noStroke()
        fill(150, 195, 125)
    
    # center and spin toroid
    translate(width/2, height/2, -100)

    rotateX(pcs.frameCount * PI / 150)
    rotateY(pcs.frameCount * PI / 170)
    rotateZ(pcs.frameCount * PI / 90)

    #  initialize point arrays
    vertices = new PVector[pts+1]
    vertices2 = new PVector[pts+1]

    #  fill arrays
    for i in [0...pts+1]
        v = vertecies[i] = new PVector()
        v2 = vertecies2[i]  = new PVector()
        v.x = latheRadius + sin(radians(angle))*radius
        if isHelix
            v.z = cos(radians(angle)) * radius - (helixOffset * segments) / 2
        else
            v.z = cos(radians(angle)) * radius
        
        angle += 360.0/pts
    

    #  draw toroid
    latheAngle = 0
    for i in [0...segments + 1]
        beginShape(QUAD_STRIP)
        for j in [0...pts+1]
            v = vertecies[j]
            v2 = vertecies2[j]

            if i > 0
                vertex(v2.x, v2.y, v2.z)
            
            v2.x = cos(radians(latheAngle))*v.x
            v2.y = sin(radians(latheAngle))*v.x
            v2.z = v.z
            #  optional helix offset
            if isHelix
                v.z+=helixOffset
            
            vertex(v2.x, v2.y, v2.z)
        
        #  create extra rotation for helix
        if isHelix
            latheAngle += 720.0 / segments
        else
            latheAngle += 360.0 / segments

        endShape()


###
# left/right arrow keys control ellipse detail
# up/down arrow keys control segment detail.
# 'a','s' keys control lathe radius
# 'z','x' keys control ellipse radius
# 'w' key toggles between wireframe and solid
# 'h' key toggles between toroid and helix
###

keyPressed = ->
  if key == CODED
      # pts
      if keyCode == UP
          if pts < 40
              pts++
      else if keyCode == DOWN
          if pts > 3
              pts--

      #  extrusion length
      if keyCode == LEFT
          if segments > 3
              segments--
      else if keyCode == RIGHT
          if segments < 80
              segments++
  
  #  lathe radius
  if key == 'a'
      if latheRadius > 0
          latheRadius--
  else if key == 's'
      latheRadius++
  
  #  ellipse radius
  if key == 'z'
      if radius > 10
          radius--
  else if key == 'x'
      radius++
  
  #  wireframe
  if key == 'w'
      if isWireFrame
          isWireFrame = false
      else
          isWireFrame = true
      
  
  #  helix
  if key == 'h'
      if isHelix
          isHelix = false
      else
          isHelix=true
      
  


