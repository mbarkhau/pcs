prevX = prevY = 0
pressed = false

mousePressed = () ->
    if pcs.mouseButton == RIGHT
        background(0)
    else
        pressed = true
    
mouseReleased = () ->
    pressed = false

setup = ->
    background(0)
    stroke(255)
    fill(255)
    strokeWeight(10)
    smooth()
    textSize(70)
    textAlign(CENTER)
    text("click to draw", pcs.width/2, pcs.height/3)
    text("right click to clear", pcs.width/2, pcs.height/2)

draw = ->
    x = pcs.mouseX
    y = pcs.mouseY

    if pressed and x > 0
        line(x, y, prevX, prevY)

    prevX = x
    prevY = y

