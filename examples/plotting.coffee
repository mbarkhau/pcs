
default_opts = {
    min_x: -5
    max_x: 5
    min_y: -5
    max_y: 5
    min_z: -2
    max_z: 2
    colors: [
        0
        color(200, 0, 0)
        color(0, 200, 0)
        color(0, 0, 200)
        color(150, 150, 0)
        color(0, 150, 150)
        color(150, 0, 150)
    ]
    samples: 4
    grid: 0
    static: false
}

w = h = x_factor = y_factor = 0
min_x = max_x = min_y = max_y = min_z = max_z = 0


# Palette courtesy of James Henstridge
# http://blogs.gnome.org/jamesh/2011/03/08/javascript-fractal/
wrap = (x) ->
    x = ((x + 256) & 0x1ff) - 256
    if x < 0
    	x = -x
    return x

palette = for i in [0...1024]
    color(wrap(7*i), wrap(5*i), wrap(11*i))


trans_x = (x) -> (x-min_x) * x_factor
trans_y = (y) -> h - ((y-min_y) * y_factor)
trans_z = (z) -> palette[z]


drawGrid = ->
    stroke(0, 100)
    
    line(0, trans_y(0), w, trans_y(0))
    line(trans_x(0), 0, trans_x(0), h)
    

plotter = (drawfunc='2d', func=max, opts={}) ->
    
    for own k, v of default_opts
        if not opts[k]?
            opts[k] = v

    if typeof func == 'function'
        funcs = [func]
    else
        funcs = func

    w = pcs.width
    h = pcs.height
    
    min_x = opts.min_x
    max_x = opts.max_x
    min_y = opts.min_y
    max_y = opts.max_y
    min_z = opts.min_z
    max_z = opts.max_z
    
    samples = opts.samples
    colors = opts.colors
    
    x_factor = w / (max_x - min_x)
    y_factor = h / (max_y - min_y)
    
    step_x = ((max_x - min_x) / w ) / samples
    step_y = ((max_y - min_y) / h )

    drawGrid()
    
    cur_x = min_x
    cur_y = max_y
    
    draw2d = ->
        next_x = cur_x + step_x * (w/8)
        for f, i in funcs
            stroke(colors[i] or 0, 255/samples)
            for x in [cur_x...next_x] by step_x
                point(trans_x(x), trans_y(f(x)))
            
        cur_x = next_x

        if cur_x > max_x
            noLoop()

    
    res = 8
    
    noStroke()
    draw3d = ->
        if res == 1
            for cur_x in [min_x...max_x] by (step_x * res)
                z = func(cur_x, cur_y)
                stroke(trans_z(z))
                point(trans_x(cur_x), trans_y(cur_y))
        else
            for cur_x in [min_x...max_x] by (step_x * res)
                z = func(cur_x, cur_y)
                fill(trans_z(z))
                rect(trans_x(cur_x), trans_y(cur_y), res, res)
        
        cur_y -= (step_y * res)
        
        if cur_y < min_y
            if res > 1
                res /= 8
                cur_y = max_y
            else
                noLoop()

    fn = 'draw' + (drawfunc or '2d').toLowerCase()
    pcs.draw = {draw2d, draw3d}[fn]
    

exports = {
    plot2D: -> plotter('2d', arguments...)
    plot3D: -> plotter('3d', arguments...)
    drawGrid
}


### test code ###

mandelbrot = (cr, ci) ->
    zi = zr = 0
    for n in [0...1024]
        a = zr*zr
        b = zi*zi
        
        if (a + b) > 100
            return n
        
        # z = z² + c
        zi = 2*zr*zi + ci
        zr = a - b + cr
        
    
    return 0


#plotter('2d', [exp, sqrt, sin, cos, log])
#plotter('3d', mandelbrot, {min_x: -2.5, min_y: -1.5, max_x: 1.5, max_y: 1.5})




