###
# amoebaAbstract_01_formatik
# Copyright 2003 Marius Watz
# http://www.evolutionzone.com/
#
# Variation on abstract computational animation for the exhibition 
# "Abstraction Now", Künstlerhaus Vienna, 29.08-28.09 2003.
#
# You are allowed to play with this code as much as you like, but
# you may not publish pieces based directly on it. The Vec2D class 
# can be used freely in any context.
###

debug = -> println((for a in arguments then a).join(", "))

px = py = -1

num = 150

w = h = 0
        
auto = []
particles = []

initialised = false
doClear = false

scMod = fadeAmount = fadeInterval = 0

randint = (a, b) -> floor(random(a, b))

setup = ->
    background(255)
    smooth()
    frameRate(60)
    
    w = pcs.width
    h = pcs.height
    
    rectMode(CENTER_DIAMETER)
    ellipseMode(CENTER_DIAMETER)
    
    particles = for i in [0...num] then new Particle()
    
    reinit()


draw = ->
    if doClear
        background(255)
        doClear = false
    
    noStroke()

    if (pcs.frameCount % fadeInterval) == 0
        fill(255, fadeAmount)
        _x = w/2
        _y = h/2
        rect(_x, _y, w, h)

    for a in auto then a.update()
    
    for p in particles when p.age > 0 then p.update()


mousePressed = reinit = ->
    doClear = true
    scMod = random(1, 1.4)
    fadeInterval = randint(5, 20)
    fadeAmount = random(1, 3)
    
    for p in particles then p.age = -1
    
    auto = for i in [0...randint(3, 6)] then new AutoMover()
    debug("reinit", fadeInterval, auto.length, particles.length)


class AutoMover

    constructor: -> @reinit()
    
    reinit: ->
        @ageGoal = randint(150, 350)
        
        if random(100) > 95
            @ageGoal *= 1.25
            
        @age = -randint(50, 150)
        @pos = new Vec2D(random(w - 100) + 50, random(h - 100) + 50)
        
        @dir = randint(360)
        @type = 0
        if random(100) > 60
            @type = 1
        
        @interval = randint(2, 5)
        
        if @type == 1
            @interval = 1
            _y = @pos.y - h/2
            _x = @pos.x - w/2
            @dir = degrees(atan2(-_y, _x))
    
    
        @dirD = random(1, 2)
        if random(100) < 50
            @dirD *= -1
            
        @speed = random(3, 6)
        @sc = random(0.5, 1)
        
        if random(100) > 90
            @sc = random(1.2, 1.6)
            
        @dirCnt = random(20, 35)
    
        if @type == 0
            @sc = if random(100) > 95
                random(1.5, 2.25)
            else
                random(0.8, 1.2)
        
        @sc *= scMod
        @speed *= @sc

    update: ->
        @age += 1
        return if @age < 0
        
        if @age > @ageGoal
            @reinit()
        
        if @type == 1
            @pos.add(cos(radians(@dir)) * @speed,
                     sin(radians(@dir)) * @speed)
                       
            @dir += @dirD
            @dirD += random(-0.2, 0.2)
            @dirCnt -= 1
            
            if @dirCnt < 0
                @dirD = random(1,5)
                if random(100) < 50
                    @dirD *= -1
                    
                @dirCnt = randint(20, 35)
                
        if @age % @interval == 0
             @newParticle()
                
        if @pos.x < -50 or @pos.x > w + 50 or @pos.y < -50 or @pos.y > h + 50
            @reinit()
            
    newParticle: ->
        if @type == 0
            @dir = floor(random(36)) * 10
        
        for p in particles when p.age < 1
            offs = random(30, 90)
            if random(100) > 50
                offs *= -1
            
            p.init(@dir+offs, @pos.x, @pos.y, @sc)
            
            break
        
        px = pcs.mouseX
        py = pcs.mouseY


class Particle

    constructor: ->
        @v = new Vec2D()
        @vD = new Vec2D()
        @age = 0

    init: (@dir, mx, my, @sc) ->
        @v.set(mx, my)
        
        prob = random(100)
        @age = if prob < 80
            15 + randint(30)
        else if prob < 99
            45 + randint(50)
        else
            100 + randint(100)
        
        @speed = random(2) + (if random(100) < 80 then 0.5 else 2)
    
        @dirMod = if random(100) < 80 then 20 else 60
        
        @initMove()
        
        @stateCnt = 10
        @col = if random(100) > 50 then 0 else 1
        
        @type = floor(random(30000)) % 2
        
        
    initMove: ->
        if random(100) > 50
            @dirMod *= -1
        
        @dir += @dirMod
        
        @vD.set(@speed, 0)
        @vD.rotate(radians(@dir + 90))
    
        @stateCnt = 10 + randint(5)
        if random(100) > 90
            @stateCnt += 30

    update: ->
        @age -= 1
        
        if @age >= 30
            @vD.rotate(radians(1))
            @vD.mul(1.01)
        
        @v.add(@vD)
        
        if @col == 0
            fill(255 - @age, 0, 100, 150)
        else
            fill(100, 200 - (@age/2), 255 - @age, 150)

        if @type == 1
            if @col == 0
                fill(255 - @age, 100, 0, 150)
            else
                fill(255, 200 - (@age/2), 0, 150)

        pushMatrix()
        scale(@sc)
        translate(@v.x, @v.y)
        rotate(radians(@dir))
        rect(0, 0, 1, 16)
        popMatrix()
        
        if @age == 0
            if random(100) > 50
                fill(200, 0, 0, 200)
            else
                fill(0, 200, 255, 200)
                
            _size = 2 + random(4)
            if random(100) > 95
                _size += 5
            
            _size *= @sc
            ellipse(@v.x * @sc, @v.y * @sc, _size, _size)
        
        if @v.x < 0 or @v.x > w or @v.y < 0 or @v.y > h
            @age = 0
        
        if @age < 30
            @stateCnt -= 1
            if @stateCnt == 0
                @initMove()


###
# General vector class for 2D vectors
###
class Vec2D

    constructor: (x=0, y=0) -> @set(x, y)
        
    set: (x, y) ->
        if not y?
            # assume x is another vector
            y = x.y
            x = x.x
        
        @x = x
        @y = y

    add: (x, y) ->
        if not y?
            y = x.y
            x = x.x

        @x += x
        @y += y

    sub: (x, y) ->
        if not y?
            y = x.y
            x = x.x

        @x -= x
        @y -= y

    mul: (m) ->
        @x *= m
        @y *= m

    div: (m) ->
        @x /= m
        @y /= m
    
    normalise: ->
        l = @length()
        return if l == 0
        
        @x /= l
        @y /= l

    rotate: (val) ->
        cosval = cos(val)
        sinval = sin(val)
        tmpx = @x*cosval - @y*sinval
        tmpy = @x*sinval + @y*cosval
    
        @x = tmpx
        @y = tmpy

    length: -> sqrt(@x*@x + @y*@y)

    angle: -> atan2(@y, @x)

    tangent: -> new Vec2D(-@y, @x)

