###
Example code adapted from http://processingjs.org/learning
###

delay = 50
X = pjs.width  / 2
Y = pjs.height / 2

###
Setup the Processing Canvas
###
setup = () ->
  strokeWeight( 10 )
  fill(0, 100, 200)     # Set fill-color to blue
  stroke(255)           # Set stroke-color white

###
Main draw loop
###
draw = () ->
  background( 100 )     # overwrite previous content
  
  deltaX = pjs.mouseX-X
  deltaY = pjs.mouseY-Y
  delta  = sqrt((deltaX*deltaX) + (deltaY*deltaY))
  
  # Track circle to new destination
  X += deltaX / delay
  Y += deltaY / delay
  
  radius = 50 + (delta / 8) + sin( pjs.frameCount / 20 ) * 10
  
  ellipse( X, Y, radius, radius ) # Draw circle


