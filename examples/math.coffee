
###
# General vector class for 2D vectors
###
class Vec2D

    constructor: (x=0, y=0) -> @set(x, y)
        
    set: (x, y) ->
        if not y?
            # assume x is another vector
            y = x.y
            x = x.x
        
        @x = x
        @y = y

    add: (x, y) ->
        if not y?
            # assume x is another vector
            y = x.y
            x = x.x

        @x += x
        @y += y

    sub: (x, y) ->
        if not y?
            # assume x is another vector
            y = x.y
            x = x.x

        @x -= x
        @y -= y

    mul: (m) ->
        @x *= m
        @y *= m

    div: (m) ->
        @x /= m
        @y /= m
    
    normalise: ->
        l = @length()
        return if l == 0
        
        @x /= l
        @y /= l

    rotate: (val) ->
        cosval = cos(val)
        sinval = sin(val)
        tmpx = @x*cosval - @y*sinval
        tmpy = @x*sinval + @y*cosval
    
        @x = tmpx
        @y = tmpy

    length: -> sqrt(@x*@x + @y*@y)

    angle: -> atan2(@y, @x)

    tangent: -> new Vec2D(-@y, @x)


###
# returns the derivative for functions with one parameter
# fdx(
#     fn,    # the function to derive
#     dx/eps # precision of derived values
#            # default: 1e-3
# )
###
fdx = (fn, eps=1e-3) ->
    return (x) -> (fn(x+eps) - fn(x)) / eps


fib = (n) -> if n < 2 then n else fib(n-1) + fib(n-2)
fib = _.memoize(fib)

sum = (ar) -> _.reduce(ar, ((a,b) -> a + b))


class ComplexNumber
    constructor: (@r, @i) ->

    add: (c) -> new ComplexNumber(@r + c.r, @i + c.i)
    sub: (c) -> new ComplexNumber(@r - c.r, @i - c.i)
    mul: (c) -> new ComplexNumber(@r*c.r - @i*c.i, @r*c.i + @i*c.r)
    modSquared: -> @r*@r + @i*@i


###
# Provides eq,ne,lt,gt,gte,lte methods based on cmp method of subclass
#
# a.cmp(b) must return the following:
#         -1 if a < b
#          0 if a == b
#         +1 if a > b
#  undefined if a and b are not comparable
###
class Compareable
    
    eq: (n) ->
        r = @cmp(n)
        if not r? then undefined else r == 0

    ne: (n) ->
        r = @cmp(n)
        if not r? then undefined else r != 0

    lt: (n) ->
        r = @cmp(n)
        if not r? then undefined else r < 0

    gt: (n) ->
        r = @cmp(n)
        if not r? then undefined else r > 0

    lte: (n) ->
        r = @cmp(n)
        if not r? then undefined else not r > 0

    gte: (n) ->
        r = @cmp(n)
        if not r? then undefined else not r < 0
        

reverse = (x) -> x.split("").reverse().join("")



# TODO: Bigint or Arbitrary precision numbers
#
# http://www.leemon.com/crypto/BigInt.js
# https://github.com/silentmatt/javascript-biginteger/blob/master/biginteger.js
# https://github.com/dankogai/js-math-bigint/blob/master/bigint.js
# https://github.com/downloads/whatgoodisaroad/Big-js/Big.js
# http://jsfromhell.com/classes/bignumber


class BigInt extends Compareable
    
    constructor: (val=0, base=10) ->
        if val instanceof BigInt
            @sign = val.sign
            @val = val.val
            return
        
        if _.isNumber(val)
            val = parseInt(val).toString(36)
            base = 36
            
        if val[0] in ['-', '+']
            @sign = val[0]
            val = val[1...]
        else
            @sign = '+'
        
        @val = reverse(val)
    
    neg: ->
        n = new BigInt(this)
        n.sign = if n.sign == '+' then '-' else '-'
        return n
    
    add: (n) ->
        n = BigInt.mk(n)
        
        return this if BigInt.ZERO.val == n.val
        
    mul: (n) ->
        n = BigInt.mk(n)
        z = BigInt.ZERO
        return z if z.val in [@val, n.val]
        
    divmod: (n) ->
        n = BigInt.mk(n)
        if n == BigInt.ZERO
            throw "Division by zero."
    
    cmp: (n) ->
        n = BigInt.mk(n)
    
    div: (n) -> @divmod(n)[0]
    mod: (n) -> @divmod(n)[1]
    
    inc: -> @add(1)
    dec: -> @add(-1)
    
    sub: (n) -> @add(BigInt.mk(n).neg())
    
    plus : BigInt::add
    minus: BigInt::sub
    times: BigInt::mul
    over : BigInt::div
    
    

BigInt.mk = (n) -> if n instanceof BigInt then n else new BigInt(n)

BigInt.ZERO    = new BigInt(0)
BigInt.ONE     = new BigInt(1)


###
# Converts the operations (+, -, *, /, %, ===, !==, <, >, <=, >=) to the appropriate
# function calls (.add, .sub, .mul, .div, .mod, .eq, .ne, .lt, .gt, .lte, .gte).
#
# If you want to mix Number Types within a function, you will need to add such
# methods to their protoype.
#
# examples of equivalent definitions:
#     oo = math.op_overload
#
#     fn = oo (n) -> n * n
#     fn =    (n) -> n.mul(n)
#
#     fn = oo (a, b, c) -> a + b * c
#     fn =    (a, b, c) -> a.add(b.mul(c))
#
#     fn = oo (a, b, c) -> (a + b) * c
#     fn =    (a, b, c) -> a.add(b).mul(c)
###
op_overload = (fn) ->
        
    all = fn.toString() # may not work on all browsers
    
    body = all.substring(all.indexOf("{") + 1, all.lastIndexOf("}"))
    
    body = body.replace(/\/\/.*/, "")            # remove normal comments
    body = body.replace(/\/\*[\s\S]*\*\//m, "")  # remove block comments
    
    # reformulate a += b to a = a + (b)
    body = body.replace(/(\w+)\s?([\+\-\*\%\|\&\/])\=\s?([\s\S]+?);/g, "$1 = $1 $2 ($3);")
    
    println body
    
    opmap = {
        '*': 'mul'
        '/': 'div'
        '%': 'mod'
        '+': 'add'
        '-': 'sub'
        '<' : 'lt'
        '>' : 'gt'
        '<=': 'lte'
        '>=': 'gte'
        '===': 'eq'
        '!==': 'nq'
    }
    
    ops = ['*', '/', '%', '+', '-', '<', '>', '<=', '>=', '===', '!==']

    opre = {}
    for o in ops
        opre[o] = new RegExp("(\\w+?)\\s?\\#{o}\\s?([^;]+)", 'g')
    
    transform = (s) ->
        for o in ops
            s = s.replace(opre[o], "$1.#{opmap[o]}($2)")
        return s
    
    
    stack = []
    res = ""
    tok = ""
    
    for ch in body
        if ch == "("
            stack.push(tok)
            tok = ""
        else if ch == ")"
            tok = stack.pop() + transform(tok)
            if stack.length == 0
                res += tok
                tok = ""
        else
            tok += ch
    
    res += tok
    res = for line in res.split("\n")
        transform(line)
        #line
    return res.join("\n") #new Function(body)


setup = ->
    println("overloaded: " + op_overload("""{
        var n = 0, m = 0;
        // a comment that shouldn't be seen
        n += 1;
        /*
         a block comment
         that shouldn't be seen
        */
        m *= 3 + (n * n) - 4;
        return m < n;
    }
    """))
    println "------"



exports = {
    Vec2D
    ComplexNumber
    fib
    sum
    fdx
}




