setup = ->
    strokeWeight(2)

draw = ->
    w = pcs.width
    h = pcs.height
    background(20)
    stroke(0, 156, 255)
    line(random(w),random(h),random(w),random(h))
