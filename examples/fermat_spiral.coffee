###
# Fermat's spiral || Vogel's Floret Model
# play with the scale and color
###
 
i = 1
j = 0
scale = 2.6
    
radius = 0
theta = 0
xpos = 0
ypos = 0

PHI = (1 + sqrt(5)) / 2
D_PHI = PHI * PHI
 
setup = ->
    noStroke()
    smooth()
    ellipseMode(CENTER)

draw = ->
    if xpos > pcs.width - 20 or pcs.height - 20 < ypos
        noLoop()
        
    i = j++
    radius = sqrt(i) * scale
    theta = i * (2 * PI) / D_PHI
    xpos = (cos(theta) * radius) + pcs.width / 2
    ypos = (-sin(theta) * radius) + pcs.height / 2
    
    r = ypos % (radius - xpos)
    g = ypos % (theta - radius)
    b = xpos % (ypos - theta)
    fill(color(r, g, b))
    ellipse(xpos, ypos, scale, scale )

