###
# from @jason_johnson
#
# http://www.unmatchedstyle.com/podcast/processingjs-hello-world-3.php
###

particles = []

draw = ->
    background(0)
   
    for p in particles
        p.update()
        p.draw()
   
    particles = _.reject(particles, (p) -> p.y > pcs.height)


mouseMoved = ->
    particles.push(new Particle(pcs.mouseX, pcs.mouseY))


class Particle

    constructor: (@x, @y) ->
        @gravity = 1
        @xV = random(-10, 10)
        @yV = random(-15, -20)
  
    update: ->
        @yV += @gravity

        @x += @xV
        @y += @yV

    draw: ->
        ellipse(@x, @y, 10, 10)

