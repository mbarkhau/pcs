###
# Wave Pendulum by David Guttman
# 
# https://github.com/davidguttman/wave-pendulum
###

CLICK_COUNT = 0

class Ball

    constructor: (opts) ->
        @ball_id = opts.ball_id

        @o_x = opts.o_x
        @o_y = opts.o_y

        @b_x = opts.b_x

        @r = opts.r || 100
        @G = 0.2
        @theta = 0.8*Math.PI/2

        # (@b_x - @o_x)/@r = Math.sin(@theta)

        @ball_r = 12

        @vel = opts.vel || 0
        @accel = opts.accel || 0

    draw: ->
        @accel = (-1 * @G / @r) * Math.sin(@theta)
        @vel += @accel
        @vel *= 1
        @theta += @vel

        @x_off = (@r * Math.sin(@theta))
        @y_off = (@r * Math.cos(@theta))

        @b_x = @o_x + @x_off
        @b_y = @o_y + @y_off

        @x_ratio = @x_off/@r
        @y_ratio = @y_off/@r

        @scaled_x = @x_ratio * (pcs.height/4) + @o_x
        @scaled_y = @y_ratio * (pcs.height/4) + @o_y

        if @x_ratio <= 0.025 and @x_ratio >= -0.025
            @active = true
        else
            @active = false

        style = CLICK_COUNT % 4

        switch style
            when 0
                    x = @scaled_x
                    y = @scaled_y
            when 1
                    x = @b_x
                    y = @o_y + pcs.height/4
            when 2
                    x = @b_x
                    y = @scaled_y
            when 3
                    x = @b_x
                    y = @b_y

        if @active
            fill 10
            stroke 100
            strokeWeight 10
            br = @ball_r * 4
            ellipse(@o_x, y, br, br)
        else
            br = @ball_r

        noStroke()
        fill(255)
        ellipse(x, y, @ball_r, @ball_r)


n_balls = 16
balls = []

reset_balls = ->
        l = pcs.width / 3
        space = (pcs.height - l/2) / n_balls
        balls = for num in [1..n_balls]
            new Ball({
                            ball_id: num-1
                            o_x: pcs.width / 2
                            o_y: space * num - (space)
                            b_x: l
                            r  : l * Math.pow((16/(16+(num-1))),2)
                    })


setup = ->
    background(0)
    reset_balls()
    frameRate(60)
    reset_balls()

draw = ->
    fade()
    for ball in balls
        ball.draw()

fade = ->
    stroke(0, 0)
    fill(0, 90)
    rect(0, 0, pcs.width, pcs.height)


mouseClicked = ->
    CLICK_COUNT += 1
    # reset_balls()
    # balls.push new Ball pcs, 
    #     o_x: pcs.mouseX
    #     o_y: pcs.mouseY


