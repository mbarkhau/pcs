###
# RWB by Andor Salga
###
h = pcs.height
w = pcs.width

SIZE = h/4

setup = ->
    frameRate(60)

draw = ->
    n = pcs.frameCount
    
    noStroke()
    fill(255, 0, 0, 5)
    rect(0, 0, w, h)

    stroke(0)
    strokeWeight(3.0)

    fill(255, 15)

    # move to the center
    translate(w/2.0, h/2.0)

    # bounce up and down
    translate(0.0, sin(n/20.0) * 30.0)

    ## rotate around center
    rotate(cos(n/100.0) * 3.0 )

    ## give some random movement
    translate(
        cos(n/20.0) * 20.0,
        sin(n/30.0) * 20.0
    )

    ## draw in center
    rect(-SIZE/2, -SIZE/2, SIZE, SIZE)

