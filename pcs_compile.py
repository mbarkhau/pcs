#!/usr/bin/env python

import os
import pyinotify

BASEPATH = "/home/mbarkhau/workspace/pcs/"


def watch_path(path, callback, condition=lambda e: True):
    class EventHandler(pyinotify.ProcessEvent):

        def process_default(self, event):
            if condition(event):
                callback(event)

    wm = pyinotify.WatchManager()
    notifier = pyinotify.Notifier(wm, EventHandler())

    p = pyinotify
    MASK = p.IN_DELETE | p.IN_CREATE | p.IN_MODIFY 
    wdd = wm.add_watch(path, MASK, rec=True)
    notifier.loop()

if __name__ == '__main__':
    def condition(e):
        return e.mask & pyinotify.IN_MODIFY and e.name.endswith('.coffee')

    def compile(e):
        print "changed: %s" % os.path.join(e.path, e.name)
        os.system(BASEPATH + "compile.sh")

    watch_path(os.path.abspath(BASEPATH + "scripts"), compile, condition)
