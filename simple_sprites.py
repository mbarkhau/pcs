#!/usr/bin/env python

import os, sys
import Image

min_css = lambda (s): s.replace("    ", "").replace("\n", "") + "\n"

BASE_CSS = min_css("""
.sprite
{
    display:inline-block;
    overflow:hidden;
    width: %(w)dpx;
    height: %(h)dpx;
    text-align: left !important;
    text-indent: -99999px;
    background-repeat:no-repeat;
    background-image:url(%(url)s);
}
""")

ICON_PLAIN = min_css("""
.s_%(name)s
{
    background-position:%(x)dpx %(y)dpx;
}
""")

ICON_SIZED = min_css("""
.s_%(name)s
{
    background-position:%(x)dpx %(y)dpx;
    width:%(w)dpx;
    height:%(h)dpx;
}
""")

def calc_offsets(objs):
    """
    Packs subareas of objects into a minimalish area.
    objects must have a size property of (width, height)
    
    The returned width and height are the size of the overall area
    within which the subareas will fit without overlap when positioned ath
    their offset. (left, top)
    
    See http://pollinimini.net/files/RectanglePacker.js
    
    [o,o...] -> [(x,y,o), (x,y,o)...]
    returns (width, height, [(x,y,o),..])
    """
    dims = [o.size + (o,) for o in objs]
    def area(dim):
        w, h, o = dim
        return w * h
    
    dims = sorted(dims, area)
    dims.reverse() # largest first
    width = reduce(lambda dims, w: max(dims[0], w), 0)
    raise Exception("Not implemented")
    

def generate(path, media_path='/img/', sprite_file='sprites.png', css_file='sprites.css'):
    """ Creates a sprite and css file in the specified directory
    
    This assumes a fixed width and height of 16px for the images.
    TODO: 
        - find the widest image
        - find icons with common width and height
          and use as default width + height in css
        - lookup 'bin packing problem' 
          probably use First Fit Decreasing
        
    path        : The filesystem path to your media files
    media_path  : The url from which the media files are served
    sprite_file : The name of the sprite image (placed in path)
    css_file    : The name of the css file (placed in path)
    """
    if not os.path.exists(path):
        print "No such directory: %s" % path
        return
    
    sprite_url  = media_path + sprite_file
    sprite_file = os.path.join(path, sprite_file)
    css_file    = os.path.join(path, css_file)
    
    files = os.listdir(path)
    images = [f for f in files if f.endswith('png') and not sprite_file.endswith(f)]
    if not images:
        print "No images found in %s" % os.path.abspath(path)
        return
    
    print "compiling %d icons" % len(images)
    
    master = Image.new(mode='RGBA', 
                       size=(16, 18 * len(images)),
                       color=(0,0,0,0))  # fully transparent
    
    icon_css = []
    for i, f in enumerate(images):
        img_path = os.path.join(path, f)
        img_name = f.split('.')[0]
        offset = i * 18
        arg = {'name': img_name, 'x': 0, 'y':-offset}
        icon_css.append(ICON_PLAIN % arg)
        image = Image.open(img_path)
        master.paste(image,(0, offset))

    master.save(sprite_file)
    print "created %s" % sprite_file
    
    with open(css_file, 'w') as css:
        arg = {'url': sprite_url, 'w': 16, 'h': 16 } 
        css.write(BASE_CSS % arg)
        css.write("".join(icon_css))

    print "created %s" % css_file
    
USAGE = """Usage:
    1. Put the images you want to sprite in a directory
    2. Run simple_sprite.py in that directory/pass it the path
    3. profit...
    
    > simple_sprite.py /path/to/icons 
    compiling 22 icons
    created /path/to/icons/sprites.png
    created /path/to/icons/sprites.css

    You may need to modify background-image:url(/img/sprites.png)
    in sprites.css depending on your webserver configuration.
    
    In your page you can use the images like so:
    <span class="ss_sprite ss_{{example_icon}}></span>   # ommit .png
    
    Depends on PIL (python image library)
    Works only with .png files
"""
def main(args): 
    if len(args) == 1:
        path = '.'
    else:
        path = args[1]
        
    if not path or path in ('-h', '--help'):
        print USAGE
    else:
        generate(path)
    
if __name__ == '__main__':
    try:
        main(sys.argv)
    except Exception, e:
        print e
        
